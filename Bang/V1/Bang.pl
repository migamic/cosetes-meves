# Uses
	use warnings;
	use strict;

	use LWP::Simple;
	use List::Util 'shuffle';
	use Term::ANSIColor;
	#use PAR::Packer;

# Llista d'arrays
	my @Nom;
	my @LletraInicial;
	my @Final;
	my @Partida;
	my @Cartes;
	my @CartesBarrejades;

# Noms cartes
	for (my $i = 0; $i < 25; $i++) {
		$Cartes[$i] = "Bang!";
	}
	for (my $i = 0; $i < 10; $i++) {
		$Cartes[$i+25] = "Fallaste!";
	}
	for (my $i = 0; $i < 5; $i++) {
		$Cartes[$i+35] = "Cerveza";
	}
	$Cartes[40] = "Saloon";
	$Cartes[41] = "Saloon";
	$Cartes[42] = "Almac".chr(130)."n";
	$Cartes[43] = "Almac".chr(130)."n";
	$Cartes[44] = "Almac".chr(130)."n";
	$Cartes[45] = "Diligencia";
	$Cartes[46] = "Diligencia";
	$Cartes[47] = "Wheels Fargo";
	$Cartes[48] = "Wheels Fargo";
	$Cartes[49] = "P".chr(134)."nico";
	$Cartes[50] = "P".chr(134)."nico";
	$Cartes[51] = "Ingenua explosiva";
	$Cartes[52] = "Ingenua explosiva";
	$Cartes[53] = "Ametralladora Gatling";
	$Cartes[54] = "Ametralladora Gatling";
	$Cartes[55] = "Indios";
	$Cartes[56] = "Indios";
	$Cartes[57] = "Indios";
	$Cartes[58] = "Duelo";
	$Cartes[59] = "Duelo";
	$Cartes[60] = "Duelo";
	$Cartes[61] = "Mustang";
	$Cartes[62] = "Mustang";
	$Cartes[63] = "Mira telesc".chr(162)."pica";
	$Cartes[64] = "Mira telesc".chr(162)."pica";
	$Cartes[65] = "Barril";
	$Cartes[66] = "Barril";
	$Cartes[67] = "Prisi".chr(162)."n";
	$Cartes[68] = "Prisi".chr(162)."n";
	$Cartes[69] = "Dinamita";
	$Cartes[70] = "Volcanic";
	$Cartes[71] = "Volcanic";
	$Cartes[72] = "Schofield";
	$Cartes[73] = "Schofield";
	$Cartes[74] = "Remington";
	$Cartes[75] = "Remington";
	$Cartes[76] = "Carabina";
	$Cartes[77] = "Carabina";
	$Cartes[78] = "Winchester";
	$Cartes[79] = "Winchester";




# Introducció de jugadors
	print "\n\n\nNombre de jugadors: ";
	my $NreJugadors = <>;

	CheckPlayerNumber();


# Llista de rols i personatges
	my @Rols = ("Sheriff", "Foragido", "Foragido", "Renegado", "Alguacil", "Foragido", "Alguacil");
	my @Personatges = ("Bart Cassidy", "Black Jack", "Calamity Janet", "El Gringo", "Jesse Jones", "Jourdonnais", "Kit Carlson", "Lucky Duke", "Paul Regret", "Pedro Ramirez", "Rose Doolan", "Sid Ketchum", "Slab the Killer", "Suzy Lafayette", "Vulture Sam", "Willy the Kid");

	@Nom = shuffle(@Nom);
	@Personatges = shuffle(@Personatges);

print "\n\n\n**************************************************\n\n\n";
print "Prem ENTER per repartir personatges";
<STDIN>;

# Repartiment de personatges
	print "\n\n\n***REPARTIMENT DE PERSONATGES***\n\n";

	sleep (2);

	CharGiving();

print "\nPrem ENTER per repartir rols";
<STDIN>;

# Repartiment de rols
	print "\n\n\n***REPARTIMENT DE ROLS***\n\n";

	sleep (2);

	RoleGiving();


sub CheckPlayerNumber {
	if ($NreJugadors > 7) {
	print "ERROR -> El nombre m".chr(133)."xim de jugadors ".chr(130)."s 7\n";
	exit;
	}
	elsif ($NreJugadors < 4) {
	print "ERROR -> El nombre m".chr(161)."nim de jugadors ".chr(130)."s 4\n";
	exit;
	}

	for (my $j = 0; $j < $NreJugadors; $j++) {
	print "Nom jugador ".($j+1)." -> ";
	$Nom[$j] = <STDIN>;
	chomp $Nom[$j];
	}
}

sub CharGiving {
	sleep (1);
	for (my $i = 0; $i < $NreJugadors; $i++) {
	
	my $Article;

	@LletraInicial[$i] = substr($Nom[$i], 0, 1);
	if ($LletraInicial[$i] eq "A" || $LletraInicial[$i] eq "E" || $LletraInicial[$i] eq "I" || $LletraInicial[$i] eq "O" || $LletraInicial[$i] eq "U") {
		$Article = "A l'";
	}
	else
	{
		$Article = "Al ";
	}

	@Final[$i] = $Article.$Nom[$i]." li toca ser ".$Personatges[$i]."\n";
	}

	@Final = shuffle(@Final);

	for (my $m = 0; $m < $NreJugadors; $m++) {
	
	print $Final[$m];
	sleep (1);
	}
}

sub RoleGiving {
	sleep(1);

	for (my $i = 0; $i < $NreJugadors; $i++) {
	$Partida[$i] = $Personatges[$i]." (".$Nom[$i].") ".chr(130)."s ".$Rols[$i]."\n";
	}

	@Partida = shuffle(@Partida);

	for (my $i = 0; $i < $NreJugadors; $i++) {
	
	print $Partida[$i];
	sleep (1);
	}
}

print "\nPrem ENTER per repartir cartes";
<STDIN>;
print "\n\n";


# Torn inicial

@CartesBarrejades = shuffle(@Cartes);
for (my $i = 0; $i < $NreJugadors; $i++) {
	sleep(1);
	print "A ".$Personatges[$i]." (".$Nom[$i].") li toca '".@CartesBarrejades[$i+(4*$i)]."', '".@CartesBarrejades[($i+1)+(4*$i)]."', '".@CartesBarrejades[($i+2)+(4*$i)]."' i '".@CartesBarrejades[($i+3)+(4*$i)]."'\n";
}

#print $Personatges[0]." (".$Nom[0].") agafa dues cartes de la pila i li toquen: ".$Cartes[int(rand(42))]." i ".$Cartes[int(rand(42))];

