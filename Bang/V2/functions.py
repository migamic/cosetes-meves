import data_parser as dp
import random
import deck
import game
### I/O FUNCTIONS ###

# Returns the input read from the user
def read_input(prompt='>>>'):
    user_in = input(prompt + ' ')
    return user_in


# Prints the given message on the screen
def display_message(message, prompt='> '):
    print(prompt,message, sep='')


# Asks for a player's name, checks if it's correct and returns it
def ask_target(g_players):
    player = get_player_idx(g_players, read_input('Target player:'))
    if player == -1:
        display_message('Incorrect player name')
        return ask_target(g_players)
    elif not g_players[player].is_alive():
        display_message('Target is dead!')
        return ask_target(g_players)
    else:
        return player


### SETTINGS CHECK ###
# Return True if check passed (i.e. input is correct)

# Check that the number of players is a valid value
def check_n_players(players):
    try:
        players = int(players)
    except:
        return False

    return players > 3 and players < 8



### GAME UTILITIES ###

# Check if a card is on the hand
def card_in_hand(card, hand_list):
    if type(card) == type('str'):
        card = dp.get_card_id(card)
    return (card in hand_list)

# Returns the index of a player by name
def get_player_idx(g_players, name):
    for i,p in enumerate(g_players):
        if p.get_name() == name: return i
    return -1

# Returns the index of the next (alive) player
def get_next_player(pl_idx, g_players):
    i = (pl_idx+1)%len(g_players)
    while not g_players[i].is_alive():
        i = (i+1)%len(g_players)
    return i

# Returns the total number of players that are still alive
def get_num_alive(g_players):
    count = 0
    for p in g_players:
        if p.get_lives() > 0:
            count += 1
    return count

# Returns the index of the player after removing dead ones
def get_alive_idx(g_players, pl_A):
    new_idx = 0
    for i,p in enumerate(g_players):
        if i == pl_A:
            return new_idx
        if p.get_lives() > 0:
            new_idx += 1
    return -1


# Returns the distance from player A to player B
# Note that it is not symmetric!
def player_distance(g_players, pl_A, pl_B):
    if pl_A == pl_B: return 0

    num_alive = get_num_alive(g_players)
    if type(pl_A) == type('str'):
        pl_A = get_player_idx(pl_A)
    if type(pl_B) == type('str'):
        pl_B = get_player_idx(pl_B)

    # Update indices to ignore dead players
    pl_A = get_alive_idx(g_players, pl_A)
    pl_B = get_alive_idx(g_players, pl_B)

    if pl_A == -1 or pl_B == -1:
        raise Exception('Tried to compute distance to a dead player')

    # Compute circular distance
    i = (pl_A - pl_B) % num_alive
    j = (pl_B - pl_A) % num_alive
    dist =  min(i, j)

    # Take into account modifiers
    if card_in_hand('Mustang', g_players[pl_B].get_modifiers()):
        dist += 1
    if card_in_hand('Scope', g_players[pl_A].get_modifiers()):
        dist -= 1

    return dist

# Returns true iff the player B is in shooting range of player A
def in_range(g_players, pl_A, pl_B):
    distance = player_distance(g_players, pl_A, pl_B)

    return distance <= g_players[pl_A].get_gun_distance()

# Returns true iff the event happened with probability p
def draw_probability(p, message=None):
    if message is not None:
        uinp = read_input(message)
        if uinp == '':
            return random.uniform(0,1) < p
        else:
            return draw_probability(p, message)


# Refills the draw deck if it is low on cards
def refill_deck(g_cards):
    if g_cards[0].get_num_cards() < 15:
        display_message('Refilling the draw deck!')

        g_cards[1].shuffle() # Shuffle the discard deck before adding it
        g_cards[0].refill(g_cards[1].get_cards())
        g_cards[0] = deck.Deck(cards=[], is_secret=False, can_add=False)


# Returns True iff any game ending condition is fulfilled
def game_ended(g_players):
    enemies_alive = False
    for p in g_players:
        if p.get_role() == dp.get_role_id('Sheriff'):
            if not p.is_alive(): return True
        elif p.get_role() == dp.get_role_id('Bandit') or p.get_role() == dp.get_role_id('Renegade'):
            if p.is_alive(): enemies_alive = True
    return enemies_alive



# Checks if a player just died and announces it
def pl_died(pl_idx, g_players):
    p = g_players[pl_idx]
    if not p.is_alive():
        role = dp.get_role_attribute(p.get_role(), 'name')
        display_message(f'{p.get_name()} ({role}) died!')

        if game_ended(g_players):
            game.display_results(g_players)
        else:
            display_message('The game goes on!')
