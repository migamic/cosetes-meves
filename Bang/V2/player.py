import data_parser as dp
import functions as fn

class Player:

    def __init__(self, character, role, max_lives, name=''):

        # Personal info
        self.name = name

        # Game character info
        self.character = character
        self.role = role

        # Game status info
        self.max_lives = max_lives
        self.lives = self.max_lives
        self.modifiers = []
        self.hand = []
        self.weapon = None

        # Game card status
        self.bang_played = False
        self.in_jail = False
        self.dynamite = False


    def get_name(self):
        return self.name

    def get_character(self):
        return self.character

    def get_role(self):
        return self.role

    def get_max_lives(self):
        return self.max_lives

    def get_lives(self):
        return self.lives

    def is_alive(self):
        return self.get_lives() > 0

    def get_gun_distance(self):
        if self.weapon is None: return 1
        return dp.get_card_attribute(self.weapon, 'shrange')

    def get_modifiers(self):
        return self.modifiers

    def get_weapon(self):
        return self.weapon

    def get_hand(self):
        return self.hand

    def get_num_hand(self):
        return len(self.hand)

    def get_bang_played(self):
        return self.bang_played

    def get_in_jail(self):
        return self.in_jail

    def get_dynamite(self):
        return self.dynamite

    def set_weapon(self, card_id):
        self.weapon = card_id

    def set_bang_played(self, status):
        self.bang_played = status

    def set_in_jail(self, status):
        self.jail = status

    def set_dynamite(self, status):
        self.dynamite = status

    def add_card(self, card_id):
        self.hand.append(card_id)

    def remove_card(self, card_name):
        card_id = dp.get_card_id(card_name)
        self.hand.remove(card_id)

    def add_modifier(self, card_id):
        self.modifiers.append(card_id)

    def remove_modifier(self, card_name):
        card_id = dp.get_card_id(card_name)
        self.modifiers.remove(card_id)

    def lose_life(self, amount=1):
        self.lives -= amount
        if self.lives < 0:
            self.lives = 0

    def gain_life(self, amount=1):
        self.lives += amount
        if self.lives > self.max_lives:
            self.lives = self.max_lives
