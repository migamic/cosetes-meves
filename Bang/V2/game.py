import player, deck
import data_parser as dp
import functions as fn
import actions as ac


# Initial message and first user input
def display_intro():
    fn.display_message('Welcome to Bang!')
    uinp = fn.read_input('Press Enter to continue. \'q\' to quit.')

    if  uinp == '':
        return
    elif uinp == 'q':
        exit()
    else:
        display_intro()


# Ask the user for the number of players (AI or not)
def ask_n_players():
    players = fn.read_input('Number of players:')
    if fn.check_n_players(players):
        return players
    else:
        # If wrong input, ask again
        fn.display_message('Incorrect number of players. Must be between 4 and 7')
        return ask_n_players()


# Print all the current game parameters and give the option to go back
def params_summary(params):
    fn.display_message('Summary of the game')
    fn.display_message('Players: '+str(params['players']))
    uinp = fn.read_input('Press Enter to continue. Press \'b\' to go back')
    if uinp == '':
        return params
    elif uinp == 'b':
        # Start the setup anew and return the updated parameters
        return setup_parameters()
    else:
        return params_summary(params)


# High level setup function
def setup_parameters():
    fn.display_message('Game set up')

    # Ask the user about each parameter
    players = ask_n_players()

    # Let the user review parameters and continue/revert
    params = {'players' : players}
    #params = params_summary(params)
    return params


# Returns a list of Player object
def setup_players(g_params):
    n_players = int(g_params['players'])

    # Get players' names
    name_list = []
    for p in range(n_players):
        name_list.append(fn.read_input(f'Name of player {p+1}:'))

    # Get a list of roles
    role_list = []
    for r in range(4):
        role_list.extend([r]*dp.get_role_attribute(r, f'amount{n_players}'))

    # Create a shuffled deck with the roles
    role_deck = deck.Deck(cards=role_list, is_secret=True, can_draw=True)
    role_deck.shuffle()

    # Create player objects with their corresponding attributes
    g_players = []
    for p in range(n_players):
        role = role_deck.draw()
        g_players.append(player.Player(
                             character='dummy',
                             role=role,
                             max_lives = 4+dp.get_role_attribute(role, 'lives_diff'),
                             name=name_list[p]))

    return g_players


# Creates a shuffled deck with game cards and gives some to every player
def setup_cards(g_players):

    # Get a list with replicated card names
    card_list = []
    num_cards = 20 # Total number of unique cards
    for c in range(num_cards):
        card_list.extend([c]*dp.get_card_attribute(c, 'amount'))

    # g_cards is a tuple (draw deck, discard deck)
    g_cards = (deck.Deck(cards=card_list, is_secret=True, can_add=False, can_draw=True),
               deck.Deck(cards=[], is_secret=False, can_add=True))
    g_cards[0].shuffle()

    # Give each player the initial cards
    for p in g_players:
        for c in range(p.get_max_lives()):
            p.add_card(g_cards[0].draw())

    return g_cards

# High level setup function
def game_setup():
    g_params = setup_parameters()
    g_players = setup_players(g_params)
    g_cards = setup_cards(g_players)

    return g_params, g_players, g_cards


# Return the index of the player with the role of Sheriff
def get_sheriff_idx(g_players):
    for i in range(len(g_players)):
        if g_players[i].get_role() == dp.get_role_id('Sheriff'):
            return i
    assert False, 'No Sheriff among the players'


# Ask the user for input and perform the corresponding action
def turn_action(pl_idx, g_params, g_players, g_cards):
    pl_name = g_players[pl_idx].get_name()

    action_dict = {
            'end' : ac.end,
            'hand' : ac.hand,
            'check' : ac.check,
            'checkall' : ac.checkall,
            'play' : ac.play,
            'discard' : ac.discard,
            'help' : ac.help,
            }

    fn.display_message('Possible actions: end, hand, check <player>, checkall, play <card>, discard <card>, help <card>')
    uinp = fn.read_input(f'{pl_name} > ').split(' ')
    act = uinp[0]
    if len(uinp) ==  1:
        user_pick = None
    else:
        user_pick = ' '.join(uinp[1:])

    if act in action_dict.keys():
        if user_pick is None:
            cont = action_dict[act](pl_idx, g_params, g_players, g_cards)
        else:
            cont = action_dict[act](pl_idx, g_params, g_players, g_cards, user_pick)

        if cont:
            turn_action(pl_idx, g_params, g_players, g_cards)
    else:
        fn.display_message('Error: Incorrect action')
        turn_action(pl_idx, g_params, g_players, g_cards)


# Adds the two top cards to the current player's hand
def initial_draw(pl_idx, g_params, g_players, g_cards):
    uinp = fn.read_input('Press Enter to draw two cards.')

    if  uinp == '':
        cards = (g_cards[0].draw(), g_cards[0].draw())
        names = [dp.get_card_attribute(c,'name') for c in cards]
        fn.display_message(f'Your two new cards are {names[0]} and {names[1]}.')
        g_players[pl_idx].add_card(cards[0])
        g_players[pl_idx].add_card(cards[1])
    else:
        initial_draw(pl_idx, g_params, g_players, g_cards)


# Check / set player status before the beginning of each turn
def initial_turn_status(pl_idx, g_params, g_players, g_cards):
    g_players[pl_idx].set_bang_played(False)

    # Check dynamite
    if fn.card_in_hand('Dynamite', g_players[pl_idx].get_modifiers()):
        explode = fn.draw_probability(10/(4*13), 'You have the dynamite! Press \'Enter\' to try your luck')
        if explode:
            fn.display_message('It exploded! You lose 3 live points!')
            g_players[pl_idx].lose_life(3)
            g_players[pl_idx].remove_modifier('Dynamite')
            g_cards[1].add(dp.get_card_id('Dynamite'))
            fn.pl_died(pl_idx, g_players)
        else:
            # Pass it to the next player
            next_pl = fn.get_next_player(pl_idx, g_players)
            fn.display_message(f'It did not explode. Now {g_players[next_pl].get_name()} has it.')
            g_players[pl_idx].remove_modifier('Dynamite')
            g_players[next_pl].add_modifier(dp.get_card_id('Dynamite'))

    # Check jail
    if fn.card_in_hand('Jail', g_players[pl_idx].get_modifiers()):
        get_out = fn.draw_probability(1/4, 'You are in jail! Press \'Enter\' to try your luck')
        if get_out:
            fn.display_message('You managed to get out of jail!')
        else:
            fn.display_message('You have to stay in jail and lose a turn.')

        # Remove the jail card
        g_players[pl_idx].remove_modifier('Jail')
        g_cards[1].add(dp.get_card_id('Jail'))
        return get_out
    return True


# High level main game function
def game_turns(g_params, g_players, g_cards):
    pl_idx = get_sheriff_idx(g_players) # The current player
    fn.display_message(f'{g_players[pl_idx].get_name()} is the Sheriff!')


    while True:
        fn.refill_deck(g_cards)

        fn.display_message(f'{g_players[pl_idx].get_name()}\'s turn!')

        start_turn = initial_turn_status(pl_idx, g_params, g_players, g_cards)

        if start_turn:
            initial_draw(pl_idx, g_params, g_players, g_cards)
            turn_action(pl_idx, g_params, g_players, g_cards)

        pl_idx = fn.get_next_player(pl_idx, g_players)


def display_results(g_players):
    print('dummy result')
    exit()



if __name__=="__main__":
    display_intro()
    g_params, g_players, g_cards = game_setup()
    game_turns(g_params, g_players, g_cards)
