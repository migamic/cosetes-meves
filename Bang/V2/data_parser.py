import csv
import pandas

### READING CSV's ###

# Returns the read csv as a Pandas data frame
def read_csv(filename):
    #with open(filename, mode ='r') as file:
    #       csvFile = csv.DictReader(file)
    csvFile = pandas.read_csv(filename)
    return csvFile



### CARDS ###

# Returns the value of a given card id
def get_card_attribute(card_id, attribute):
    card_info = read_csv('cards.csv')
    return card_info.loc[card_id, attribute]

# Returns the id of a given card name
def get_card_id(card_name):
    card_info = read_csv('cards.csv')
    result =  card_info.loc[card_info.name == card_name, 'id']
    if result.empty: return None
    return int(result)



### ROLES ###

# Returns the value of a given role id
def get_role_attribute(role_id, attribute):
    role_info = read_csv('roles.csv')
    return role_info.loc[role_id, attribute]

# Returns the id of a given role name
def get_role_id(role_name):
    role_info = read_csv('roles.csv')
    result =  role_info.loc[role_info.name == role_name, 'id']
    if result.empty: return None
    return int(result)

