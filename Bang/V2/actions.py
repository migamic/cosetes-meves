import player, deck
import data_parser as dp
import functions as fn


def end(pl_idx, g_params, g_players, g_cards):
    # Check that the player does not have excess cards
    n_cards = g_players[pl_idx].get_num_hand()
    n_lives = g_players[pl_idx].get_lives()
    if n_cards > n_lives:
        fn.display_message(f'Cannot end turn: the number of cards in the hand ({n_cards}) is greater than the number of live points ({n_lives})')
        return True
    return False

def hand(pl_idx, g_params, g_players, g_cards):
    cards = ', '.join([dp.get_card_attribute(c,'name') for c in g_players[pl_idx].get_hand()])
    fn.display_message(f'Your cards ({g_players[pl_idx].get_num_hand()}): {cards}')
    return True



def help(pl_idx, g_params, g_players, g_cards, user_pick=None):
    if user_pick is None:
        fn.display_message('You must choose a card to see its description')
        return True

    c_id = dp.get_card_id(user_pick)
    if c_id is None:
        fn.display_message(f'Card \'{user_pick}\' is not in the game!')
        return True

    fn.display_message(f'Card: {user_pick}', '')
    c_type = dp.get_card_attribute(c_id, 'type')
    fn.display_message(f'Type: {c_type}', '')
    c_amount = dp.get_card_attribute(c_id, 'amount')
    fn.display_message(f'Amount: {c_amount}', '')
    c_description = dp.get_card_attribute(c_id, 'description')
    fn.display_message(f'Usage: {c_description}', '')

    return True

def check(pl_idx, g_params, g_players, g_cards, user_pick=None):

    if user_pick is None:
        target_idx = pl_idx
    else:
        target_idx = fn.get_player_idx(g_players, user_pick)
    p = g_players[target_idx]

    fn.display_message(f'{p.get_name()} ({p.get_character()})', '')
    if p.get_role() == dp.get_role_id('Sheriff') or p.get_lives() == 0 or pl_idx == target_idx:
        role = dp.get_role_attribute(p.get_role(), 'name')
        fn.display_message(f'Role: {role}', '')
    else:
        fn.display_message('Role unknown','')
    fn.display_message(f'Live points: {p.get_lives()}/{p.get_max_lives()}', '')
    fn.display_message(f'Number of cards in hand: {p.get_num_hand()}', '')
    mods = ', '.join([dp.get_card_attribute(c,'name') for c in p.get_modifiers()])
    if mods == '': mods = 'None'
    weapon = p.get_weapon()
    weapon = 'None' if weapon is None else dp.get_card_attribute(weapon, 'name')
    fn.display_message(f'Modifiers played: {mods}', '')
    fn.display_message(f'{p.get_name()}\'s weapon: {weapon} (shooting range of {p.get_gun_distance()})', '')
    fn.display_message(f'Your distance to {p.get_name()}: {fn.player_distance(g_players, pl_idx, target_idx)}', '')
    fn.display_message(f'{p.get_name()}\'s distance to you: {fn.player_distance(g_players, target_idx, pl_idx)}', '')

    return True

def checkall(pl_idx, g_params, g_players, g_cards):
    fn.display_message('Public information of all the players')
    fn.display_message('Use \'check <player name>\' for more detailed info')

    for p in g_players:
        fn.display_message('**************************', '')
        fn.display_message(f'{p.get_name()} ({p.get_character()})', '')
        if p.get_role() == dp.get_role_id('Sheriff') or p.get_lives() == 0:
            role = dp.get_role_attribute(p.get_role(), 'name')
            fn.display_message(f'Role: {role}', '')
        else:
            fn.display_message('Role unknown','')
        fn.display_message(f'Live points: {p.get_lives()}/{p.get_max_lives()}', '')

    return True

def play(pl_idx, g_params, g_players, g_cards, user_pick=None):
    card_actions = {
            'Bang!': card_bang,
            'Beer' : card_beer,
            # 'Cat Balou' : card_catbalou,
            # 'Duel' : card_duel,
            # 'Gatling' : card_gatling,
            # 'General Store' : card_genstore,
            # 'Indians!' : card_indians,
            # 'Missed!' : card_missed,
            # 'Panic!' : card_panic,
            'Saloon' : card_saloon,
            'Stagecoach' : card_stagecoach,
            'Wells Fargo' : card_wellsfargo
            }

    if user_pick == None:
        fn.display_message(f'You must choose a card to play!')
        return True

    if not fn.card_in_hand(user_pick, g_players[pl_idx].get_hand()):
        fn.display_message(f'Card \'{user_pick}\' is not in your hand!')
        return True

    if user_pick in {'Cat Balou', 'Duel', 'Gatling', 'General Store', 'Indians!', 'Missed!', 'Panic!'}:
        fn.display_message(f'Card \'{user_pick}\' is not implemented yet')
        return True

    card_type = dp.get_card_attribute(dp.get_card_id(user_pick),'type')
    if card_type == 'action':
        correct = card_actions[user_pick](pl_idx, g_params, g_players, g_cards)
    elif card_type == 'modifier':
        correct = play_modifier(pl_idx, g_params, g_players, g_cards, user_pick)
    elif card_type == 'weapon':
        correct = play_weapon(pl_idx, g_params, g_players, g_cards, user_pick)
    else:
        correct = False

    if correct:
        g_players[pl_idx].remove_card(user_pick)
        g_cards[1].add(dp.get_card_id(user_pick))
    else:
        fn.display_message(f'Action cancelled')

    return True

def discard(pl_idx, g_params, g_players, g_cards, user_pick=None):
    if user_pick == None:
        fn.display_message(f'You must choose a card to discard!')
        return True
    if fn.card_in_hand(user_pick, g_players[pl_idx].get_hand()):
        g_players[pl_idx].remove_card(user_pick)
        g_cards[1].add(dp.get_card_id(user_pick))
        fn.display_message(f'\'{user_pick}\' has been discarded')
    else:
        fn.display_message(f'Card \'{user_pick}\' is not in your hand!')
    return True


### CARD ACTIONS ###

def card_bang(pl_idx, g_params, g_players, g_cards):

    if g_players[pl_idx].get_bang_played() == True:
        fn.display_message('A Bang! card was already played in this turn')
        return False

    target = fn.ask_target(g_players)

    if target == pl_idx:
        fn.display_message('Target cannot be the same player')
        return False

    # Check if target is in range
    if not fn.in_range(g_players, pl_idx, target):
        fn.display_message('Target is not in your shooting range')
        return False


    # Ask target to play Missed!

    # Remove life point from target
    fn.display_message(f'{g_players[target].get_name()} lost a life point!')
    g_players[target].lose_life()
    fn.pl_died(target, g_players)

    # Set bang_played to true unless player has a Volcanic
    if not g_players[pl_idx].get_weapon() == dp.get_card_id('Volcanic'):
        g_players[pl_idx].set_bang_played(True)

    return True

def card_beer(pl_idx, g_params, g_players, g_cards):

    # Check that life points are not max already
    if g_players[pl_idx].get_lives() == g_players[pl_idx].get_max_lives():
        fn.display_message('You already have full health!')
        return False

    # Else add a life point
    g_players[pl_idx].gain_life()
    fn.display_message('+1 life point')
    return True

def card_saloon(pl_idx, g_params, g_players, g_cards):
    for p in g_players:
        if p.is_alive() and p.get_lives() < p.get_max_lives():
            p.gain_life()
            fn.display_message(f'{p.get_name()} gained 1 life point')
    return True

def card_stagecoach(pl_idx, g_params, g_players, g_cards):
    cards = (g_cards[0].draw(), g_cards[0].draw())
    names = [dp.get_card_attribute(c,'name') for c in cards]
    fn.display_message(f'Your two new cards are {names[0]} and {names[1]}.')
    g_players[pl_idx].add_card(cards[0])
    g_players[pl_idx].add_card(cards[1])
    return True

def card_wellsfargo(pl_idx, g_params, g_players, g_cards):
    cards = (g_cards[0].draw(), g_cards[0].draw(), cards[0].draw())
    names = [dp.get_card_attribute(c,'name') for c in cards]
    fn.display_message(f'Your two new cards are {names[0]}, {names[1]} and {names[2]}.')
    g_players[pl_idx].add_card(cards[0])
    g_players[pl_idx].add_card(cards[1])
    g_players[pl_idx].add_card(cards[2])
    return True

def card_jail(pl_idx, g_params, g_players, g_cards):
    target = fn.ask_target(g_players)

    if target == pl_idx:
        fn.display_message('Target cannot be the same player')
        return False

    if g_players[target].get_role() == dp.get_role_id('Sheriff'):
        fn.display_message('The Sheriff cannot be put in jail')
        return False

    if fn.card_in_hand('Jail', g_players[target].get_modifiers()):
        fn.display_message('The target player is already in jail!')
        return False

    # Add Jail as a modifier to the target player
    g_players[target].add_modifier(dp.get_card_id('Jail'))
    fn.display_message(f'{g_players[target].get_name()} has been put in jail')
    return True


### OTHER CARD TYPES ###

def play_weapon(pl_idx, g_params, g_players, g_cards, user_pick):

    if g_players[pl_idx].get_weapon() is not None:
        weapon = dp.get_card_attribute(g_players[pl_idx].get_weapon(), 'name')
        inp = fn.read_input(f'Discard current weapon: {weapon}? (y/n)')
        if inp != 'y': return False
        g_cards[1].add(dp.get_card_id(g_players[pl_idx].get_weapon()))

    g_players[pl_idx].set_weapon(dp.get_card_id(user_pick))
    return True


def play_modifier(pl_idx, g_params, g_players, g_cards, user_pick):

    if user_pick == 'Jail':
        return card_jail(pl_idx, g_params, g_players, g_cards)

    # Check that the same modifier is not played already
    if fn.card_in_hand(user_pick, g_players[pl_idx].get_modifiers()):
        fn.display_message(f'You already have one instance of \'{user_pick}\' active!')
        return False

    # Add the modifier
    g_players[pl_idx].add_modifier(dp.get_card_id(user_pick))
    return True
