from collections import deque
import random

class Deck:

    def __init__(self, cards=[], is_secret=False, can_add=False, can_draw=False):

        # Cards (first element is top)
        self.cards = deque()
        [self.cards.append(c) for c in cards]

        # Deck info
        self.is_secret = is_secret
        self.can_add = can_add
        self.can_draw = can_draw



    # Getter functions
    def get_cards(self):
        return list(self.cards)

    def get_num_cards(self):
        return len(self.cards)

    def get_is_secret(self):
        return self.is_secret

    def get_can_add(self):
        return self.can_add

    def get_can_draw(self):
        return self.can_draw

    def get_top_card(self):
        if self.get_is_secret():
            raise Exception('Cannot get top card of secret deck')
        return self.cards[-1]



    # Possible actions on the deck

    # Returns the top card and removes it from the deck
    def draw(self):
        if not self.can_draw:
            raise Exception('Cannot draw from this deck')
        if self.get_num_cards() < 1:
            raise Exception('Cannot draw from an empty deck')

        drawn_card = self.cards.pop()
        return drawn_card

    # Given a card, adds it on top of the deck
    def add(self, card):
        if not self.can_add:
            raise Exception('Cannot add on this deck')
        self.cards.append(card)

    # Reorders the current cards of the deck randomly
    def shuffle(self):
        random.shuffle(self.cards)

    # Adds a list of cards on the bottom
    def refill(self, cards):
        self.cards.extendleft(cards)
