# Bang!

A text-based implementation of the card game [Bang!](https://en.wikipedia.org/wiki/Bang!_(card_game)).

V1 is a very poorly coded version that I did back in the day. The code quicky became unmanageable and I gave it up eventually.

V2 is my second go at this challenge. I took a much more modular approach and, although it is still in its beginnings, it is already more advanced than V1 in functionality. The idea behind it is to serve as the backend for the game, where clients can be built to make it easier for humans to play (maybe even make it online), or it can serve as testing ground for AI agents competing to win the game.
