import urllib.request

# Retorna la ID a partir d'un enllaç de torneig
def getID(url):
    separat = url.split("/tnr")
    if len(separat) == 1:
        return -1
    tornID = separat[1][:6]
    print(tornID)
    return int(tornID)

# Retorna HTML de la web donada
def gethtml(url):
    resposta = urllib.request.urlopen(url)
    return resposta.read()

# Retorna True sii la ronda existeix (hi ha emparellaments)
def existeix_ronda(html):
    return str(html).find(";snr=") != -1 # ";snr= va abans de cada jugador


# Retorna el nom del torneig
def nom_torneig(html):
    separat = str(html).split("title")
    if len(separat) < 3:
        return "Torneig desconegut"
    return separat[1][48:-6] # Neteja les etiquetes HTML i nomes retorna el nom


# Retorna una llista ordenada segons l'emparellament dels jugadors
def llistajugs(html):
    separats = str(html).split("snr")
    del separats[0] # Elimina el primer tros de text

    # Neteja el nom de tots els separats
    jugadors = [None] * len(separats)
    for j in range(len(separats)):
        jugadors[j] = separats[j].split('>')[1][:-4]

    return jugadors


# Escriu els emparellaments de la ronda donats els jugadors
def emparellaments(jugadors, punts=False, elo=False, resultats=False, curt=False):
    jugs=jugadors.copy() # Per a poderlo editar sense modificar l'original

    if len(jugs)%2 == 1:
        jugs.append("bye")

    if curt:
        jugs = [j.split()[0] for j in jugs]

    maxlen = len(max(jugs, key=len))

    empar = ""
    for i in range(round(len(jugs)/2)):
        empar = empar + ('{:>2} {:>{width}} {:^1} {:<{width}}'.format(i+1, jugs[2*i], "-", jugs[2*i+1], width=maxlen))+ '\n'

    return empar
