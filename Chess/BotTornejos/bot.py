from uuid import uuid4
from telegram.ext import Updater, CommandHandler
import data as dt

# Llegir info de la configuracio
def llegir_config(usuari,valor):
    with open("./configs/"+usuari+".conf", "r") as conf_file:
        for linia in conf_file:
            if linia.startswith(valor):
                return linia.split('=')[1]
    return -1



# FUNCIONS DEL BOT

# Inicia la conversa amb el bot i crea un fitxer de configuracio
def start(update, context):
    id_usuari = update.message.from_user.id
    open("./configs/"+str(id_usuari)+".conf", "a")
    print("Configuracio creada per a l'usuari " + str(id_usuari))
    envia_missatge(update, context, "Funcionant!\nEscriu \"/ajuda\" per a veure les possibles comandes.")

def ajuda(update, context):
    missatge = '''Possibles comandes:
    - /start inicia la conversa amb el bot
    - /ajuda mostra aquesta ajuda
    - /ronda "r" mostra els emparellaments de la ronda "r"
    '''
    envia_missatge(update, context, missatge)


# Envia un missatge
def envia_missatge(update, context, missatge, plaintext = False):
    if plaintext:
        context.bot.send_message(chat_id=update.effective_chat.id, text=missatge)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text=missatge, parse_mode='HTML')


# Envia els emparellaments d'una ronda concreta
def ronda(update, context):
    if len(context.args) == 0: # L'usuari no ha especificat la ronda
        ronda = 1
    else:
        ronda = context.args[0]
    link = 'http://chess-results.com/tnr533400.aspx?lan=9&art=2&rd='+str(ronda)
    web = dt.gethtml(link)
    if dt.existeix_ronda(web):
        jugadors = dt.llistajugs(web)
        missatge = dt.nom_torneig(web) + " (<a href=\"" + link + "\">link</a>)\n"
        missatge = missatge + "Emparellaments de la <b> ronda " + str(ronda) + "</b> :\n"
        missatge = missatge + "<code>" + dt.rondaellaments(jugadors) + "</code>"
        envia_missatge (update, context, missatge)
    else:
        envia_missatge (update, context, "Ronda no disponible")

# Mostra info del torneig
def info(update,context):
    id_usuari = update.message.from_user.id
    missatge = llegir_config(str(id_usuari), "torneig")
    envia_missatge(update, context, missatge)

# Afegeix la ID del torneig a la configuracio de l'usuari
def torn(update,context):
    if len(context.args) == 0:
        envia_missatge(update, context, "Error: no hi ha cap enllaç")
    else:
        link = context.args[0]
        tornID = dt.getID(link)
        if tornID == -1:
            envia_missatge(update, context, "Error: enllaç no vàlid")
        else:
            envia_missatge(update, context, "Id afegida: " + str(tornID))


# Setup del bot
rtoken = open("token.txt", "r").read().strip()
updater = Updater(token=rtoken, use_context=True)
dispatcher = updater.dispatcher

# Crides a les funcions
start_handler = CommandHandler('start', start); dispatcher.add_handler(start_handler)
ajuda_handler = CommandHandler('ajuda', ajuda); dispatcher.add_handler(ajuda_handler)
ronda_handler = CommandHandler('ronda', ronda); dispatcher.add_handler(ronda_handler)
info_handler = CommandHandler('info', info); dispatcher.add_handler(info_handler)
torn_handler = CommandHandler('torn', torn); dispatcher.add_handler(torn_handler)

# Posa en marxa el bot
updater.start_polling()
