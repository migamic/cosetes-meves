import sys
import re
import requests
from bs4 import BeautifulSoup

def get_html(url):
    # Send a GET request to the URL and retrieve the HTML content
    response = requests.get(url)
    return response.text


def get_first_div(url):

    html_content = get_html(url)

    # Parse the HTML content using BeautifulSoup
    soup = BeautifulSoup(html_content, 'html.parser')

    # Find the first div element in the parsed HTML
    div = soup.find_all('div', class_='defaultDialog')

    return div[2]


def get_players(div, n_boards):
    td_elements = div.find_all('td', class_='CR')#, href=regex)
    info_list = [td.text for td in td_elements][6:]
    players = []
    for i in range(n_boards):
        players.append({'name': info_list[7*i + 1], 'title': info_list[7*i]})
        players.append({'name': info_list[7*i + 3], 'title': info_list[7*i+2]})

    elos = div.find_all('td', class_='CRc')#, href=regex)
    elo_list = [elo.text for elo in elos][10:]
    for i in range(n_boards*2):
        players[i]['elo'] = elo_list[4*i+2]
    return players


def output_pairings(output_file, players, n_round):
    with open(output_file, 'w') as file:
        file.write(f'[ePGN "0.1;DGT LiveChess/2.2.5"]\n')
        for i in range(int(len(players)/2)):
            pl1 = players[i*2]
            pl2 = players[i*2 + 1]

            file.write(f'[Event "47 Open Internacional Vila de Sitges"]\n')
            file.write(f'[Site "Sitges"]\n')
            file.write(f'[Date "2023.07.01"]\n')
            file.write(f'[Round "{n_round}.{i+1}"]\n')
            file.write('[White "'+pl1['name']+'"]\n')
            file.write('[Black "'+pl2['name']+'"]\n')
            file.write('[Result "*"]\n')
            if pl1['title'] not in {'','MK'}: file.write('[WhiteTitle "'+pl1['title']+'"]\n')
            if pl2['title'] not in {'','MK'}: file.write('[BlackTitle "'+pl2['title']+'"]\n')
            file.write('[WhiteElo "'+pl1['elo']+'"]\n')
            file.write('[BlackElo "'+pl2['elo']+'"]\n')
            file.write('\n*\n\n')


if __name__ == "__main__":

    if len(sys.argv) != 5:
        print(f'Usage: {sys.argv[0]} <# boards> <round> <chess-results pairings URL> <output_pgn>')
        sys.exit(1)

    n_boards = int(sys.argv[1])
    n_round = int(sys.argv[2])
    url = sys.argv[3]
    output_file = sys.argv[4]

    pairings_div = get_first_div(url)
    players = get_players(pairings_div, n_boards)
    output_pairings(output_file,players, n_round)
