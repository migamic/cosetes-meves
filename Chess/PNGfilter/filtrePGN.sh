#!/bin/bash
# Script que filtra un PGN agafant partides d'un jugador amb diverses opcions

# Cami sencer a la base
entrada=$1
sortida=$2

# Path a l'executable actual
current_path=$(dirname "$0")

read -p 'Cognoms/username: ' nom
read -p "$nom juga amb [B/N/-]:" color_input

case $color_input in
    b|B)
        query="\[White \"$nom"
    ;;
    n|N)
        query="\[Black \"$nom"
    ;;
    *)
        query="\[(White|Black) \"$nom"
    ;;
esac

echo "Filtrant partides de $nom"
cat "$entrada" | perl "$current_path"/perlFilter.pl "$query" > "$sortida"

echo 'Fet!'
