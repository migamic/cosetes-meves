use strict;
use warnings;

# Usage example, to print only games in which at least one of the players is rated between 2200-2999
# cat input.pgn | perl perlFilter.pl "\[(White|Black)Elo \"2[2-9]" > output.pgn

my @buf;
my $match = 0;
my $header = 1;
my $line; 

my $query = $ARGV[0];


# Prints the game in the buffer array
sub output_game {
    print @buf;
}


# Returns true iff the current line begins with '[' and a letter
# some special symbols are also between brackets and we want to ignore them
sub begins_with_bracket {
    return $line =~ /^\[[A-Z]/
}


# Returns true iff the current line contains the query string
sub header_match {
    return $line =~ /$query/i
}


# Process line by line the whole input file
while ($line = <STDIN>) {
    # If still processing the header
    if ($header) {
        # If it is indeed header
        if (begins_with_bracket()) {

            # Check if the player matches the query
            if (header_match()) {
                $match = 1
            }
        }
        else {
            # Else it is not header anymore
            $header = 0;
        }

        # Add current line to the buffer
        push(@buf, $line);
    }
    else {
        # If a new header appears (new game)
        if (begins_with_bracket()) {
            # Output the previous game if it matched
            if ($match) {
                output_game();
            }

            # Reset everything for the new game
            $match = 0;
            $header = 1;
            @buf = ();
            
            # Check if the player matches the query
            if (header_match()) {
                $match = 1
            }
        }
        # Else still processing the game, just append

        # Add current line to the buffer
        push(@buf, $line);
    }
}


# When there are no more lines, also print the last game if it matches!
if ($match) {
    output_game();
}
