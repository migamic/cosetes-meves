#!/bin/bash
# Un script de bash que descarrega les bases especificades de TWIC i les ajunta en un sol fitxer

liminf=919 # A partir de quina agafar (inclosa)
limsup=1345 # Fins a quina agafar (inclosa)

nom="TWICS.pgn" # Nom del fitxer de sortida (fa append)

touch ${nom}

linecounter=0
for i in $( seq ${liminf} ${limsup} )
do
    wget -q "https://www.theweekinchess.com/zips/twic${i}g.zip"

    if [ -f "./twic${i}g.zip" ]; then
        unzip -q "twic${i}g.zip"
        rm "twic${i}g.zip"
        cat "twic${i}.pgn" >> ${nom}

        linecounter=$((linecounter + $(cat "twic${i}.pgn" | wc -l)))

        rm "twic${i}.pgn"

        done=$((${i} - ${liminf} + 1))
        total=$((${limsup} - ${liminf})) 
        echo "Fet ${done} de ${total} ($((${done} * 100 / ${total}))%)"
    else
        echo "Twic no. ${i} no trobat. Ometent"
    fi

done

# Comprovar que les lines al final sigui igual que la suma de linies
if [ $linecounter == $(cat "${nom}" | wc -l) ]; then
    echo "Tot acabat bé"
else
    echo "Algun error"
fi
