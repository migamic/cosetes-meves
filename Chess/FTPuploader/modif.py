import re
import sys
from itertools import groupby


# Script to modify a PGN tags based on rules. The rules file is given as the first argument. An example is
#     WHERE Round "3.1"
#     TAG Black "Doe, John"
#     TAG Result "*"
#     GAME RESULT *
#     
#     WHERE Site "*"
#     TAG Site "Super cool site!"
#     
#     WHERE Round "3.3"
#     GAME CLEAR
#     GAME RESULT *
#     
#     WHERE Round "3.4"
#     TAG Result "1/2-1/2"
#     GAME RESULT 1/2-1/2



def read_input(inst_file):
    lines = []

    with open(inst_file, 'r') as file:
        for line in file:
            lines.append(line.rstrip())

    grouped_lines = groupby(lines, lambda x: x != '')
    return [list(group) for key, group in grouped_lines if key]


def split_string_keep_quotes(string):
    pattern = r'"[^"]+"|\S+'
    split_segments = re.findall(pattern, string)
    return split_segments


def parse_syntax(lines):
    rules = []

    for i,l in enumerate(lines):
        curr_rule = {}

        condition = split_string_keep_quotes(l[0])

        if len(condition) != 3:
            print(f'Condition of rule {i} does not have 3 elements. Aborting')
            sys.exit(1)

        if condition[0] != 'WHERE':
            print(f'Condition of rule {i} does not start with a WHERE clause. Aborting')
            sys.exit(1)

        curr_rule['condition'] = (condition[1], condition[2][1:-1])
        curr_rule['tags'] = []
        curr_rule['games'] = []

        for j,order in enumerate(l[1:]):
            words = split_string_keep_quotes(order)

            if words[0] == 'TAG':
                if len(words) != 3:
                    print(words)
                    print(f'Line {j} of rule {i} does not have 3 elements. Aborting')
                    sys.exit(1)
                curr_rule['tags'].append((words[1], words[2][1:-1]))
            elif words[0] == 'GAME':
                if len(words) < 2:
                    print(f'Line {j} of rule {i} does not have >=2 elements. Aborting')
                    sys.exit(1)
                instruction = words[1]
                if instruction == 'CLEAR':
                    if len(words) != 2:
                        print(f'Line {j} of rule {i} has too many arguments for CLEAR. Aborting')
                        sys.exit(1)
                    curr_rule['games'].append('clear')
                elif instruction == 'RESULT':
                    if len(words) != 3:
                        print(f'Line {j} of rule {i} does not have 3 arguments. Aborting')
                        sys.exit(1)
                    curr_rule['games'].append(('result', words[2]))
                else:
                    print(f'Unknown game instruction in line {j} of rule {i}. Aborting')
                    sys.exit(1)
            else:
                print(f'Unknown keyword \'{words[0]}\' in line {j} of rule {i}. Aborting')
                sys.exit(1)

        rules.append(curr_rule)

    return rules


def read_games(games_file):
    batched_lines = read_input(games_file)

    reading_tags = True

    games = []
    game = {}
    for b in batched_lines:
        if reading_tags:
            game['tags'] = []
            for l in b:
                tag_name = l[1:-1].split(' ')[0]
                tag_args = re.search(r'"([^"]*)"', l[1:-1]).group(1)
                game['tags'].append([tag_name, tag_args])
        else:
            game['moves'] = []
            for l in b:
                game['moves'].append(l)

            games.append(game)
            game = {}

        reading_tags = not(reading_tags)


    return games


def write_games(output_file, games):
    with open(output_file, 'w') as file:
        for game in games:
            for t in game['tags']:
                file.write(f'[{t[0]} "{t[1]}"]\n')
            file.write('\n')
            for m in game['moves']:
                file.write(m + '\n')
            file.write('\n')


def modify_games(rules, games):
    for r in rules:
        (tag, value) = r['condition']

        for g in games:
            for t in g['tags']:
                if t[0] == tag and t[1] == value: # Match

                    # Modify tags
                    for new_t in r['tags']:
                        tag_found = False
                        for old_t in g['tags']:
                            if old_t[0] == new_t[0]:
                                tag_found = True
                                old_t[1] = new_t[1]
                        if not tag_found:
                            # Create it new
                            g['tags'].append(list(new_t))

                    # Modify game
                    for g_rule in r['games']:
                        if g_rule == 'clear':
                            result = g['moves'][-1].split(' ')[-1]
                            g['moves'] = [result]
                        elif g_rule[0] == 'result':
                            if len(g['moves'][-1].split(' ')) == 1:
                                # Edge case where there are no blank spaces in the last line
                                g['moves'][-1] = g_rule[1]
                            else:
                                last_line = g['moves'][-1].rsplit(' ', 1)[0]
                                last_line += f' {g_rule[1]}'
                                g['moves'][-1] = last_line
                        else: assert(False)

    return games


if __name__ == "__main__":

    if len(sys.argv) != 4:
        print(f'Usage: {sys.argv[0]} <rules_file> <input_pgn> <output_pgn>')
        sys.exit(1)

    lines = read_input(sys.argv[1])

    if len(lines) == 0:
        print("No rules defined. Exiting")
        sys.exit(0)

    rules = parse_syntax(lines)
    games = read_games(sys.argv[2])
    modify_games(rules, games)
    write_games(sys.argv[3], games)

