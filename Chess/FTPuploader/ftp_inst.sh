#!/bin/bash

# Connects to a remote FTP server and performs the specified operations
# The list of operations should be adapted to the particular server, since the destination directory will likely be different

# It is intented to be called from the 'uploader.sh' script


# Check if all four arguments are provided
if [ $# -ne 4 ]; then
  echo "Usage: $0 <server> <user> <password> <local file>"
  exit 1
fi


ftp -n -v $1 << EOF
  quote USER $2
  quote pass $3
  put $4 live/games.pgn
EOF

