#!/bin/bash

# Script to regularly call the ftp_inst.sh with the given username and password
# First argument is token file with (in separate lines):
# - FTP server
# - User
# - Password
# - Upload rate (in seconds)
# Second argument is the path to games.pgn
# [Optional] rules for PGN modification

# Check if the input file is provided
if [ $# -le 1 ]; then
  echo "Usage: $0 <token_file> <games_file>"
  exit 1
fi

unset -v server ftp_user ftp_pass itvl
for var in server ftp_user ftp_pass itvl; do
  IFS= read -r "$var" || break
done < $1


echo "Starting upload routine"


while true
do
  date +%T

  if [ $# -eq 3 ]; then
      echo "Modifying PGN"
      python3 modif.py $3 $2 output-modified.pgn
      echo "Connecting via FTP"; echo
      ./ftp_inst.sh $server $ftp_user $ftp_pass output-modified.pgn
      echo; echo "Connection finished"
  else
      echo "Connecting via FTP"; echo
      ./ftp_inst.sh $server $ftp_user $ftp_pass $2
      echo; echo "Connection finished"
  fi


  echo "Waiting $itvl seconds"
  sleep $itvl
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
done
echo "Done"
