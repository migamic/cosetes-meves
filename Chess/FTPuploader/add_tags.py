import re
import sys
from itertools import groupby


# Script to generate a rules file for modif.py to put Elo and Title tags to all players
# Input (STDOUT) is a complete PGN file with all Elo and Title tags
# Output is a set of rules to add Elo and Title tags to another PGN (overwriting the existing ones, if present)

NEW_TAGS = {'WhiteElo', 'BlackElo', 'WhiteTitle', 'BlackTitle'}


def read_input(inst_file):
    lines = []

    with open(inst_file, 'r') as file:
        for line in file:
            lines.append(line.rstrip())

    grouped_lines = groupby(lines, lambda x: x != '') 
    return [list(group) for key, group in grouped_lines if key]


def read_games(games_file):
    batched_lines = read_input(games_file)

    reading_tags = True

    games = []
    game = {}
    for b in batched_lines:
        if reading_tags:
            game['tags'] = []
            for l in b:
                tag_name = l[1:-1].split(' ')[0]
                tag_args = re.search(r'"([^"]*)"', l[1:-1]).group(1)
                game['tags'].append([tag_name, tag_args])
        else:
            game['moves'] = []
            for l in b:
                game['moves'].append(l)

            games.append(game)
            game = {}

        reading_tags = not(reading_tags)


    return games


def get_existing_tags(tag_list):
    tags = set()
    for t in tag_list:
        tags.add(t[0])
    return tags


def get_tag_value(tag_list, tag_name):
    for t in tag_list:
        if t[0] == tag_name:
            return t[1]
    return None


def write_rules(games):
    for game in games:
        modified = False
        existing_tags = get_existing_tags(game['tags'])
        n_round = get_tag_value(game['tags'], 'Round')

        assert(n_round is not None) # Needed for the WHERE clause

        for nt in NEW_TAGS:
            if nt in existing_tags:
                if not modified:
                    print(f'WHERE Round "{n_round}"')
                tag_val = get_tag_value(game['tags'], nt)
                print(f'TAG {nt} "{tag_val}"')
                modified = True

        if modified:
            print()



if __name__ == "__main__":

    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} <input_pgn>')
        sys.exit(1)

    games = read_games(sys.argv[1])
    write_rules(games)
