# Thingies

Dump of little programs I coded in my free time over the years. Some of them are in Catalan.

Some of these are unfinished or very poorly coded, but I keep them for nostalgia or to serve as reference for future projects. But there are also a few that are still quite useful! Here's my pick on the most interesting ones:

* `Arduino/Volant` has the code for an ASCII game coded with the Curses library. It is meant to be played with a modified steering wheel controller where an Arduino translates physical input to serial input to the computer. Unfortunately, the only version that is playable without such controller is `prevolant.py` which is a very old and bare version of the code.
* The `Chess` directory contains a collection of utilities that I coded out of necessity but I still use extensively nowadays.
* `GraphVisualizer` is a web interactive visualizer of force-directed graph layouts.
* `Raspberry/timelapse.py` is a script to record timelapses with a Raspberry Pi camera.
* `RPSadversarial` contains the implementation of a very simple reinforcement learning agent that *learns* to play rock-paper-scissors.
