# Classics

## Cryptography

An encrypter and decrypter for any text string. It uses a cypher and a few other simple hand-coded operations. The encryption is too week to have any use in real life, but it did provide quite a bit of fun among my friends.

## Procedurals

A couple of little experiments I did for generating random "terrain".

## Celpiente

One of my first-ever programs, coded somewhere around 2014. It is my own version of the timeless *Snake* game.

## EqSegonGrau

A simple solver of quadratic equations that I coded back in high school because I was tired of doing everything by hand every time.

## Koennia

A very small text-based city simulator. The only variables the player has control over are the taxes and the budget for government spending, each of which will affect the population and economic growth.

## RandomAprox

A simple program to approximate a random value with a Monte Carlo technique.
