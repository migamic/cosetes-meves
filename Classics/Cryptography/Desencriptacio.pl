use warnings;
use strict;

print "\n\nDESENCRIPTADOR\n";

#Input

print "Text original: ";
my $TextOr = <STDIN>;
chomp $TextOr;

#Converteix la string en un array de lletres
my @OriginalCharacters = split(//, $TextOr);

my @AsciiCharacters;
my @NewAscii;
my @IntermediateAscii;
my @FinalAscii;

my $FirstModifier = ord(substr($TextOr, 0, 1));
my $SecondModifier = ord(substr($TextOr, 1, 1));
#my $Modifier = 0;


#Passa les lletres a ascii
	for (my $i = 0; $i < scalar @OriginalCharacters-2; $i++) {
		$AsciiCharacters[$i] = ord($OriginalCharacters[$i+2]);
	}


#Inverteix els valors de l'array (sense els dos modificadors inicials)
	my $ProvCounter = 0;
	for (my $k = scalar @OriginalCharacters-2; $k > 0; $k--) {
		$NewAscii[$ProvCounter] = $AsciiCharacters[$k-1];
		$ProvCounter = $ProvCounter+1;
	}


#Fa la inversió per parelles
	my $counter = 2;
	for (my $j = 0; $j < scalar @OriginalCharacters-2; $j++) {
		
		if ($counter > scalar @OriginalCharacters-2) {
			$counter = $counter -1;
			$IntermediateAscii[$j] = $NewAscii[$counter-1];
		}
		else
		{
			$IntermediateAscii[$j] = $NewAscii[$counter-1];
			if ($counter%2 == 0)
			{
				$counter = $counter - 1;
			}
			else
			{
				$counter = $counter + 3;
			}
		}
	}


#Descodifica cada lletra amb el modificador corresponent
	for (my $i = 0; $i < scalar @OriginalCharacters-2; $i++) {

		if ($i%2==0) {
			$FinalAscii[$i] = $IntermediateAscii[$i] - (7*$FirstModifier);
		}
		else
		{
			$FinalAscii[$i] = $IntermediateAscii[$i] - (7*$SecondModifier);
		}

		
		while ($FinalAscii[$i] < 97) {
			$FinalAscii[$i] = $FinalAscii[$i] + 26;
		}

		if ($IntermediateAscii[$i] == 45)
		{
			$FinalAscii[$i] = 32;
		}
	}


#Output
	print "\n\n";
	print "Text desencriptat: ";

	for (my $j = 0; $j < scalar @OriginalCharacters-2; $j++) {
		print chr($FinalAscii[$j]);
	}

	print "\n\n";
	print "\n\n";