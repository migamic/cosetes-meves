use warnings;
use strict;

print "\n\nENCRIPTADOR\n";

#Input

print "Text original: ";
my $TextOr = <STDIN>;
chomp $TextOr;


#Crea l'array de lletres
my @OriginalCharacters = split(//, $TextOr);

my @AsciiCharacters;
my @NewAscii;
my @FinalAscii;

#Un modificador aleatori
my $FirstModifier = int(rand(25))+97;
my $SecondModifier = int(rand(25))+97;
#my $Modifier = 0;


#Passa totes les lletres a ascii i els afegeix el modificador
for (my $i = 0; $i < scalar @OriginalCharacters; $i++) {
	$AsciiCharacters[$i] = ord($OriginalCharacters[$i]);

	
	if ($i%2 == 0) {
		$NewAscii[$i] = $AsciiCharacters[$i] + (7*$FirstModifier);
	}
	else
	{
		$NewAscii[$i] = $AsciiCharacters[$i] + (7*$SecondModifier);
	}

	while ($NewAscii[$i] > 122) {
		$NewAscii[$i] = $NewAscii[$i] - 26;
	}

	if(ord($OriginalCharacters[$i]) == 32)
	{
		$NewAscii[$i] = 45;
	}
}




#Output
print "\n\n";
print "Text encriptat: ";
print chr($FirstModifier);
print chr($SecondModifier);

#Inverteix les lletres per parelles
my $counter = 2;
for (my $j = 0; $j < scalar @OriginalCharacters; $j++) {
	
	if ($counter > scalar @OriginalCharacters) {
		$counter = $counter -1;
		$FinalAscii[$j] = $NewAscii[$counter-1];
	}
	else
	{
		$FinalAscii[$j] = $NewAscii[$counter-1];
		if ($counter%2 == 0)
		{
			$counter = $counter - 1;
		}
		else
		{
			$counter = $counter + 3;
		}
	}
}


#Escriu les lletres en ordre revertit
for (my $k = scalar @OriginalCharacters; $k > 0; $k--) {
	print chr($FinalAscii[$k-1]);
}


print "\n\n";
print "\n\n";