print "\n"."\n"."\n";
print "* * * * * * * * * * * * * * * * * * * * * * *\n";
print "*                                           *\n";
print "*   CALCULADORA D'EQUACIONS DE SEGON GRAU   *\n";
print "*                                           *\n";
print "*                                           *\n";
print "*            Jaume Ros (2017)               *\n";
print "* * * * * * * * * * * * * * * * * * * * * * *\n";
print "\n";
print "\n";
print "Introduir terme A: ";
$A = <>;
chomp $A;
print "\n";
print "Introduir terme B: ";
$B = <>;
chomp $B;
print "\n";
print "Introduir terme C: ";
$C = <>;
chomp $C;
print "\n";


if (($B**2)-(4*$A*$C) < 0)
{
	$Solucio = "No te solucio";
}
elsif ($A == 0) {
	$Solucio = "No es una equacio de segon grau!";
}
else
{
	$Sol1 = ((-$B) + ((($B**2)-(4*$A*$C))**(1/2)));
	$Sol2 = ((-$B) - ((($B**2)-(4*$A*$C))**(1/2)));

	if ($Sol1 == $Sol2) {
		$Solucio = "Solucio = ".$Sol1 / (2*$A)."  (solucio doble)";
	}
	else
	{
		$Solucio = "Solucio 1 = ".$Sol1 / (2*$A)."\n"."Solucio 2 = ".$Sol2 / (2*$A);
	}
}

print "\n"."****************"."\n";
print $Solucio;
print "\n"."****************";