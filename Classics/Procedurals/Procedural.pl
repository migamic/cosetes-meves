use strict;
use warnings;
use Time::HiRes ('sleep');


my $p = 9;
my $LastLength = 20;
my $NewLength;
while ($p == 9) {
	$NewLength = int(rand(6))-3;


	for (my $i = 0; $i < ($LastLength+$NewLength); $i++) {
		print ".";
	}

	$LastLength = $LastLength+$NewLength;

	if ($LastLength < 1)
	{
		$LastLength = 1;
	}
	print "\n";
	sleep(0.05);
}