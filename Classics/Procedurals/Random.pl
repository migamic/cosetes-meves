use strict;
use warnings;
use Time::HiRes ('sleep');
use Win32::Console::ANSI;
use Term::ANSIColor;

my $Stop = 0;

my $Anterior = 5;
my $Random;
my $Turn = "none";
my $TurnLimitUp = 9;
my $TurnLimitDown = 1;

while ($Stop != 1)
{
	Curve ($Turn);

	PrintTrack();

	sleep(0.05);
}

sub PrintTrack {



	if ($Turn eq "right")
	{
		for (my $i = 0; $i < 70 + $Anterior - 1; $i++) {
		print " ";
	};
	}
	elsif ($Turn eq "left")
	{
		for (my $i = 0; $i < 70 + $Anterior + 1; $i++) {
		print " ";
	};
	}
	else
	{
		for (my $i = 0; $i < 70 + $Anterior; $i++) {
		print " ";
	};
	}


	



	if (rand(3)<1)
	{
		print colored ("*", 'yellow on_green');
	}
	else
	{
		print " ";
	}

	if ($Turn eq "right")
	{
		print chr(92)."            ".chr(92);
	}
	elsif ($Turn eq "left")
	{
		print "/"."            "."/";
	}
	else
	{
		print "|"."            "."|";
	}




	
	if (rand(3)<1)
	{
		print colored ("*", 'yellow on_green');
	}

	print "\n";
}

sub Curve {

	$Random = rand(10);

	if ($Turn eq "right")
	{
		$TurnLimitUp = 4;
		$TurnLimitDown = 0;
	}
	elsif ($Turn eq "left")
	{
		$TurnLimitUp = 10;
		$TurnLimitDown = 6;
	}
	else
	{
		$TurnLimitUp = 7;
		$TurnLimitDown = 3;
	}

	if ($Random>$TurnLimitUp)
	{
			$Anterior++;
			$Turn = "right";
	}
	elsif ($Random<$TurnLimitDown)
	{
			$Anterior--;
			$Turn = "left";
	}
	else
	{
		$Turn = "none";
	}
}