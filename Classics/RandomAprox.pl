use warnings;
use strict;


print "\n\n\nSIMULADOR PER APROXIMAR NOMBRES\nJaume Ros Alonso (2018)\n\n";


print "El nombre aleatori ha de ser entre 0 i ";

my $Limit = <STDIN>;
chomp $Limit;

print "Grau d'aproximacio: ";

my $Exactitud = <STDIN>;
chomp $Exactitud;

my $Random = rand($Limit);
my $Encerts = 0;

print "\n\n Nombre inicial: ";
print $Random;
print "\n\n Prem ENTER per iniciar l'aproximacio";
<>;


for (my $i = 0; $i < $Exactitud; $i++) {
	if (rand($Limit) < $Random){
		$Encerts ++;
	}
}


my $Aproximacio = ($Encerts/$Exactitud)*$Limit;

print "\n Nombre aproximat:".$Aproximacio."\n";

if (($Random-$Aproximacio) < 0) {
	print "\n Diferencia: ".sprintf("%.2f", ($Aproximacio-$Random));
	print "\n Error relatiu: ".sprintf("%.2f", ((($Aproximacio-$Random)/$Random)*100))."%";

}
else {
	print "\n Diferencia: ".sprintf("%.2f", ($Random-$Aproximacio));
	print "\n Error relatiu: ".sprintf("%.2f", ((($Random - $Aproximacio)/$Random)*100))."%";
}

print "\n\n\n\n";