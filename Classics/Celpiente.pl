
use Win32::Console;
use Term::ReadKey;




$con = Win32::Console->new(STD_OUTPUT_HANDLE); 
$con->Alloc;

$con->Title("CELPIENTE");

$pas=1;			#Aixo serveix per al color de les bombes a la subrutina comprovarxocbomba

$dirX=0;		#Variables de direcció. Poden valer 1, 0 o -1
$dirY=0;




&fons;

$numB=0;

&rectangle(17,9,45,6);

$con->WriteChar("Benvinguts a Celpiente", 29, 10);
$con->WriteChar("Considerat el joc de l'any per dues persones", 18, 11);
$con->WriteChar("Fet per Jaume i Papa Ros", 28, 12);
$con->WriteChar("Premeu z per jugar", 31, 13);
$con->WriteChar("Premeu . dos cops per acabar", 26, 14);


$sortida="no";
while($sortida eq "no"){
	
	$limitX=79;
	$limitY=24;
	$aleatX=int(rand($limitX));
	$aleatY=int(rand($limitY));

	$hasmenjat=1;
	
	ReadMode 4; # Turn off controls keys
	$key = ReadKey();

	$tempsinicial=time();

	if ($key eq "z"){
		&partida;
	}
	
	if ($key eq "e"){
		$sortida="si";
	}
}




#S'ACABA EL PROGRAMA, COMENCEN LES SUBRUTINES


sub esborrarpantalla
{
	for($esborrarY=0;$esborrarY<25;$esborrarY++){
		for($esborrarX=0;$esborrarX<80;$esborrarX++){
			$con->WriteChar(" ", $esborrarX, $esborrarY);
		}
	}
}






sub fons
{
	for($esborrarY=0;$esborrarY<25;$esborrarY++){
		for($esborrarX=0;$esborrarX<80;$esborrarX++){
			$attrs = chr($FG_RED | $BG_YELLOW);
			$con->WriteAttr($attrs, $esborrarX, $esborrarY);
		}
	}
}





sub pintarnovabomba
{
	$bomba=chr(219);
	$aleatXB=int(rand($limitX));
	$aleatYB=int(rand($limitY));
	$numB++;
	$con->WriteChar($bomba, $aleatXB, $aleatYB);
	$xB[$numB]=$aleatXB;
	$yB[$numB]=$aleatYB;
	$attrs = chr($FG_BLUE | $BG_YELLOW);
	$con->WriteAttr($attrs, $xB[$numB], $yB[$numB]);
}




sub comprovarxocbomba
{
	$xocat="no";
	
	
	
	for($i=1;$i<$numB+1;$i++){
		if($x==$xB[$i]){
			if($y==$yB[$i]){
				$xocat="si";
			}
		}
		
	}
	
	
}




sub rectangle
{
	my ($xinicial,$yinicial,$ample,$alt)=@_;
	
		
	
	$con->WriteChar(chr(218), $xinicial, $yinicial);			#cantonada esquerra superior
	$con->WriteChar(chr(191), $ample+$xinicial, $yinicial);			#cantonada dreta superior
	$con->WriteChar(chr(192), $xinicial, $alt+$yinicial);			#cantonada esquerra inferior
	$con->WriteChar(chr(217), $ample+$xinicial, $alt+$yinicial);		#cantonada dreta inferior
	
	
	
	
	
	for($a=$xinicial+1;$a<$ample+$xinicial;$a++){				#recta horitzontal superior
		$con->WriteChar(chr(196), $a, $yinicial);
	}
	
	
	for($b=$yinicial+1;$b<$alt+$yinicial;$b++){				#recta vertical esquerra
		$con->WriteChar(chr(179), $xinicial, $b);
	}
	
	
	for($c=$xinicial+1;$c<$ample+$xinicial;$c++){				#recta horitzontal inferior
		$con->WriteChar(chr(196), $c, $alt+$yinicial);
	}
	
	
	for($d=$yinicial+1;$d<$alt+$yinicial;$d++){
		$con->WriteChar(chr(179), $ample+$xinicial, $d);		#recta vertical dreta
	}
}



sub partida
{
	
		
		
	&esborrarpantalla;
	
	$con->WriteChar("*", $aleatX, $aleatY);

	
	$attrs = chr($bomba_BLACK | $BG_YELLOW);
	
	



	$c="C";		#En honor a la Celpiente

	$y=int(rand($limitY));
	$x=int(rand($limitX));
	
	

	ReadMode 4; # Turn off controls keys
	while ($key ne ".") {
		$key = ReadKey(-1);
		
		if($pas==1){
			$pas=2;
		} else {
			$pas=1;
		}
		# $con->WriteChar("Passot: ".$pas, 1, 5);    debug
		
		
		$temps=time();
		$con->WriteChar("Temps: ", 1, 0);
		$tempsfinal=$temps-$tempsinicial;
		$con->WriteChar($tempsfinal, 1, 1);
		
		
		
		
		
		$yvella9=$yvella8;$con->WriteChar($temps-$tempsinicial, 1, 1);
		$xvella9=$xvella8;
		
		$yvella8=$yvella7;
		$xvella8=$xvella7;
		
		$yvella7=$yvella6;
		$xvella7=$xvella6;
		
		$yvella6=$yvella5;
		$xvella6=$xvella5;
		
		$yvella5=$yvella4;
		$xvella5=$xvella4;
		
		$yvella4=$yvella3;
		$xvella4=$xvella3;
		
		$yvella3=$yvella2;
		$xvella3=$xvella2;
		
		$yvella2=$yvella;
		$xvella2=$xvella;
		
		$yvella=$y;
		$xvella=$x;
		
		
		if($key eq "w"){
			$dirY=-1;
			$dirX=0;
		}
		
		if($key eq "s"){
			$dirY=1;
			$dirX=0;
		}
		
		if($key eq "a"){
			$dirX=-1;
			$dirY=0;
		}
		
		if($key eq "d"){
			$dirX=1;
			$dirY=0;
		}
		
		
		
		$x=$x+$dirX;
		$y=$y+$dirY;
		
		
		if ($x==-1){
			$x=79;
		}
		if ($x==80){
			$x=0;
		}
		if ($y==-1){
			$y=24;
		}
		if ($y==25){
			$y=0;
		}
		
		
		
		$con->WriteChar($c, $x, $y);  					#Aixo escriu la nova c
		$con->WriteChar("e", $xvella, $yvella);
		$con->WriteChar("l", $xvella2, $yvella2);
		$con->WriteChar("p", $xvella3, $yvella3);
		$con->WriteChar("i", $xvella4, $yvella4);
		$con->WriteChar("e", $xvella5, $yvella5);
		$con->WriteChar("n", $xvella6, $yvella6);
		$con->WriteChar("t", $xvella7, $yvella7);
		$con->WriteChar("e", $xvella8, $yvella8);
		$con->WriteChar(" ", $xvella9, $yvella9);		#Aixo esborra les 2 c's anteriors
		
		&comprovarxocbomba;
		
		if ($x==$aleatX){
			if ($y==$aleatY){
				$aleatX=int(rand($limitX));			#aqui es quan es menja un nombre
				$aleatY=int(rand($limitY));
				$hasmenjat++;
				$con->WriteChar("*", $aleatX, $aleatY);
				&pintarnovabomba;
			}
		}
		
						#aqui es quan es menja la bomba
			if($xocat eq "si"){
				&esborrarpantalla;
				$con->WriteChar("HAS MENJAT UNA BOMBA!", 30, 12);
				$missatge="Puntuaci".chr(162)." total: ".$hasmenjat;
				$con->WriteChar($missatge, 1, 21);
				$missatgetemps="Temps total: ".$tempsfinal." segons";
				$con->WriteChar($missatgetemps, 1, 22);
				$con->WriteChar("Prem e per sortir", 1, 23);
				$con->WriteChar("Prem z per tornar a jugar", 1, 24);
				$key=".";
			}
		select(undef,undef,undef, .05);
		
	}
}