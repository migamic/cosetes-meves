use strict;
use warnings;
use Scalar::Util qw(looks_like_number);
use Win32::Console::ANSI;
use Term::ANSIColor;

my $Any = 0;
my $LastCommand = "\n";
my $UpdateCommand = "f";
my $Population = 3058;
my $LastPopulation = 1000;
my $Money = 196000;
my $LastMoney = 200000;
my $Services = 5000;
my $Taxes = 2;

while ($LastCommand ne "S") {

	if ($LastCommand eq "\n") {
		Update();
	}
	else{
		print "ERROR: Paraula desconeguda\n";
	}


	if ($UpdateCommand eq "\n") {
		PassaAny();
		$Any++;
		$LastCommand = $UpdateCommand;
	}
	elsif ($UpdateCommand eq "S") {
		$LastCommand = $UpdateCommand;
	}
	else {
		$LastCommand = <STDIN>;
		if ($LastCommand ne "\n") {
			chomp $LastCommand;
		}
	}
	
}

sub Update {
	print "\n **************************************************";
	print "\n\n         KOENNIA\n";
	print "          Any ".$Any."\n\n";
	print " Dades generals:";
	print "\n - Poblacio: ".$Population." habitants";
	print "\n - Diners: ".$Money." PO\n";

	$UpdateCommand = <STDIN>;
	while ($UpdateCommand ne "\n") {
		chomp $UpdateCommand;

		if ($UpdateCommand eq "S") {
			last;
		}
		elsif ($UpdateCommand eq "detall") {
			Detail();
		}
		elsif ($UpdateCommand eq "editar") {
			Edit();
		}
		else{
			print "ERROR: Paraula desconeguda\n";
		}

		$UpdateCommand = <STDIN>;
	}
	
		
}

sub Detail {
	print "\ Dades detallades:";
	print "\n - Poblacio: ".$Population." habitants (".sprintf("%.2f", (($Population*100/$LastPopulation)-100))."% d'increment respecte l'any passat)";
	print "\n - Diners: ".$Money."PO (".sprintf("%.2f", (($Money*100/$LastMoney)-100))."% d'increment respecte l'any passat)";
	print "\n - Pressupost en serveis: ".$Services." PO";
	print "\n - Impostos: ".$Taxes." PO/habitant/any";
	print "\n"
}

sub Edit {
	print "\ Nou pressupost en serveis: ";
	my $Input = <STDIN>;
	if (looks_like_number($Input)) {
		$Services = $Input;
		chomp $Services;
		print "   -> El nou pressupost de serveis es ".$Services." PO a l'any\n";
	}
	elsif ($Input ne "\n") {
		print "ERROR: No es un nombre\n";
		Edit();
	}
	print "\ Impost per habitant: ";
	$Input = <STDIN>;
	if (looks_like_number($Input)) {
		$Taxes = $Input;
		chomp $Taxes;
		print "   -> La nova quantitat d'impostos es ".$Taxes." PO per habitant a l'any\n";
	}
	elsif ($Input ne "\n") {
		print "ERROR: No es un nombre\n";
		Edit();
	}
}

sub PassaAny {
	$LastMoney = $Money;
	$LastPopulation = $Population;
	$Money = $Money + ($Population*$Taxes) - $Services;
	$Population = $Population * ($Services/($Population*$Taxes));
}