import csv
from geopy.geocoders import Nominatim
import requests
from datetime import date, timedelta

def get_coords(city):
    geolocator = Nominatim(user_agent="my-app")
    location = geolocator.geocode(city)
    latitude = location.latitude
    longitude = location.longitude
    return latitude, longitude

def get_sunrise_sunset_data(date, city, lat, lon):
    url = f"https://api.sunrise-sunset.org/json?lat={lat}&lng={lon}&date={date}"
    response = requests.get(url)
    data = response.json()
    return data["results"]

def export_to_csv(start_date, end_date, city):
    with open(f"data-{city}.csv", "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["Day", "Sunrise", "Sunset"])
        current_date = start_date
        lat,lon = get_coords(city)
        print(lat,lon)
        while current_date <= end_date:
            print(current_date)
            data = get_sunrise_sunset_data(current_date, city, lat, lon)
            sunrise = data["sunrise"]
            sunset = data["sunset"]
            writer.writerow([current_date, sunrise, sunset])
            current_date += timedelta(days=1)

start_date = date(2022, 5, 1)
end_date = date(2022, 9, 30)
city = "Valletta"

export_to_csv(start_date, end_date, city)
print("Data exported successfully!")

