# Sunrise/Sunset experiments

The summer solstice is the longest day of the year, and also the shortest night. At least in latitudes not too far from the equator.

In a talk over dinner, someone mentioned that the [summer solstice is not the day of the year where the sun comes out earliest](https://earthsky.org/tonight/earliest-sunrises-before-summer-solstice/), neither the day at which it sets latest. After my initial surprise, I immediately mentioned the question: *could it happen that the longest day and shortest night of the year don't come after one another?*

People kept saying that it was obviously not possible, which motivated me to look further into it. Here are my results!


## My solution

After some time (longer that I want to admit, because it was not easy!) I was able to come up with a case in which the shortest night happens two nights earlier than the longest day of the year. Here it is:

#day | Sunrise | Sunset | Day duration | Night duration _(between day N and N+1)_
--- | --- | --- | --- | ---
1 | 07:00am | 09:15pm | 14:15h | 09:15h
2 | 06:30am | 09:30pm | 15:00h | **08:30h**
3 | 06:00am | 09:15pm | 15:15h | 08:35h
4 | 05:30am | 09:10pm | **15:20h** | 08:50h
5 | 06:00am | 09:00pm | 15:00h | 09:30h
6 | 06:30am | 08:45pm | 14:15h | *

In the example above, the shortest night happens between days #2 and #3, but the longest day is day #4. So hooray!

Note that the fact that the sunrise (and sunset) difference between one day and the next is so large is just for clarity, everything could be uniformly scaled to the order of seconds and still be valid.

### The catch?

Of course, the above case is only possible because the sunrise and sunset curves are not symmetric with one another, which was the motivation for the question in the first place. 

However, my intuition is that in real life each individual curve is more or less sinusoidal, but the sunset curve in the example above does not fulfill this condition. I have not been able to come up with any case in which the longest day and shortest night are not neighbours, and both sunrise and sunset curves are sinusoidal.

Maybe some day I will look further into this and either come up with an example, or more proper proof that it cannot happen. Future work!


## What happens in reality?

To finish this little experiment, I decided to check real sunrise/sunset data and test if this scenario happened somewhere in the world. The `sun.py` script pulls such data from public APIs and exports it in CSV format, to be analyzed by a spreadsheet software.

I analyzed a handful of cities around the world and was not able to find any positive example. So my short-tested conclusion is that, **it can theoretically happen, but does not happen in reality**.
