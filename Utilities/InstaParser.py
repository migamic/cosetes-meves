# Prints the users that are not following back, aka 'traitors'

import instaloader
import getpass

# Get instance
L = instaloader.Instaloader()

# Login or load session
username = input("Enter username: ")
password = getpass.getpass()

print('Logging in')
L.login(username, password)

# Obtain profile metadata
target = input("Enter target: ")
print('Getting target profile metadata')
profile = instaloader.Profile.from_username(L.context, target)

followers = set()
following = set()

# Print list of followers
print('Getting list of followers')
for follower in profile.get_followers():
    followers.add(follower.username)

# Print list of followees (following)
print('Getting list of following')
f_count = {}
for followee in profile.get_followees():
    following.add(followee.username)
#    f_count[followee.username] = followee.followers # This is very slow, but allows sorting

print('Intersecting sets')
diff = following.difference(followers)

# Print the unordered list of traitors
for d in diff:
    print(d)

exit() # From here on is printing them sorted by following, which requires the line above to be uncommented

# To get an ordered list, f_count from above is needed

print()
print(f'{len(diff)} traitors in total')

# Sort by number of followers
traitors = sorted(traitors, key = lambda x: x[1])

print()
print('List of traitors:')
for t in traitors:
    print(f'{t[0]} ({t[1]} followers)')
