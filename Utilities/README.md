# Utilities

## InstaParser

A script to get the username of all the people that are not following you back, a.k.a. *traitors*.

It relies on the [instaloader](https://instaloader.github.io/) module for Python.

It requires an Instagram account to log in and get the data. The target profile must either be public, followed by the user or be the user himself; i.e. the user needs to have access to the target's list of followers and followees.

> Instagram does not not seem too keen on people doing many calls to their API, so don't be surprised if some are blocked when scanning users with a large amount of followers/followees.

I also added the feature to sort *traitors* by their number of followers, so profiles with more followers are shown last, since they are usually less relevant (e.g. celebrities, which won't follow you back usually...). However, this seems to be very slow and I have only managed to get it working for small accounts.
