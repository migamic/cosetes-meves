import numpy as np
import matplotlib.pyplot as plt
from time import sleep

# The computer strategy to use
# -1 : Ask the user
# 0: Always pick the same
# 1: Random pick
# 2: Expect the user not to change
# 3: Adversarial multi-armed bandit
STRATEGY1 = 0
STRATEGY2 = 3

# Sleep time (in seconds) between games
SLEEP_TIME = 0.001

# Number of games to play
ITERATIONS = 100

names = {
    1 : 'Rock',
    2 : 'Paper',
    3 : 'Scissors'
}


# Returns the result in reward format
# Reward for the 2nd player. 1 means win, 0 tie, -1 loss
def game_result(inp1, inp2):
    reward = inp2 - inp1
    if reward == 2:
        reward = -1
    elif reward == -2:
        reward = 1

    return reward


# Prints the results of the game
def eval_game(inp1, inp2):
    print('Player 1:', names[inp1])
    print('Player 2:', names[inp2])

    reward = game_result(inp1, inp2)
    if reward > 0:
        print('Player 2 won')
    elif reward < 0:
        print('Player 1 won')
    else:
        print('Tie')


# Asks the user for the move and returns it
def user_input():
    print(names) # Display the possible options
    inp = input()
    if inp == '1' or inp == '2' or inp == '3':
        return int(inp)
    else:
        # Ask again
        print('Incorrect input')
        return user_input()


# Returns the move pick from the computer or user, depending on strategy
def move(history, strat, params, pl_id):
    if strat == -1:
        return user_input()
    elif strat == 0:
        return 2
    elif strat == 1:
        return np.random.randint(3)+1
    elif strat == 2:
        if len(history) == 0:
            # First try is random
            return np.random.randint(3)+1
        move = history[-1][0] +1
        if move == 4: move = 1
        return move
    elif strat == 3:
        if len(history) == 0:
            # Use the initial default probabilities
            return np.random.choice(3, p=params['P']) +1

        rt = game_result(history[-1][0], history[-1][1])
        if pl_id == 1:
            # The reward function assumes the agent is second.
            rt = -rt

        print('Reward:', rt)
        # Update reward estimates
        st = np.zeros(3)
        act_id = history[-1][pl_id-1] -1 # The previous action
        st[act_id] = rt/params['P'][act_id]
        params['S'] = params['S'] + st

        # Compute softmax probabilities
        e_x = np.exp(params['eta']*params['S'] - np.max(params['eta']*params['S']))
        probs = e_x / e_x.sum()

        # Update softened probs
        params['P'] = (1-params['eta'])*probs + params['eta']/3

        # Update time step
        params['t'] == params['t']+1

        # Sample action from the distribution
        return np.random.choice(3, p=params['P']) +1
    else:
        raise Exception('Invalid STRATEGY value')


# Prints the percentage of wins
def print_stats(history):
    wins1 = 0
    wins2 = 0
    for g in history:
        result = game_result(g[0], g[1])
        if result == -1:
            wins1 += 1
        elif result == 1:
            wins2 += 1

    print(f'Player 1 wins: {wins1} ({round(100*wins1/len(history),2)}%); Player 2 wins: {wins2} ({round(100*wins2/len(history),2)}%); Total games: {len(history)}')


# Returns a dict with the necessary parameters depending on the strategy
def init_player(strat):
    if strat == 3:
        params = {
            't' : 0,                    # Timestep
            'P' : np.random.rand(3),    # Arm probabilities
            'S' : np.zeros(3),          # Reward estimates
            'eta' : 0.1                 # Temperature hyperparam
        }

        # Normalize probabilities
        params['P'] = params['P']/sum(params['P'])
        return params
    else:
        return {}


# Prints a line chart with the evolution of the games
def plot_results(history):
    x_axis = [i for i in range(len(history)+1)]
    total1 = [0]
    total2 = [0]
    for h in history:
        r = game_result(h[0],h[1])
        if r == -1:
            total1.append(total1[-1]+1)
            total2.append(total2[-1])
        elif r == 1:
            total1.append(total1[-1])
            total2.append(total2[-1]+1)
        else:
            total1.append(total1[-1])
            total2.append(total2[-1])

    plt.plot(x_axis, total1, label=f'Player 1: Strat {STRATEGY1}')
    plt.plot(x_axis, total2, label=f'Player 2: Strat {STRATEGY2}')
    plt.legend()
    plt.show()



# Main loop of the game
def main():
    history = [] # History of moves that the computer can see
    p1_params = init_player(STRATEGY1)
    p2_params = init_player(STRATEGY2)

    for i in range(ITERATIONS):
        inp1 = move(history, STRATEGY1, p1_params, 1)
        inp2 = move(history, STRATEGY2, p2_params, 2)
        eval_game(inp1, inp2)
        history.append((inp1, inp2))
        print_stats(history)
        print('\n\n')
        sleep(SLEEP_TIME)

    plot_results(history)

main()
