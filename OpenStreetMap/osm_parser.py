from bs4 import BeautifulSoup
import networkx as nx
import numpy as np
import pickle


# Returns the euclidean distance between two points
def node_distance(a,b):
    return np.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)


# Returns a list of edges for nodes in distance less than thr
def edges_by_distance(pos, thr = 0.001):
    edges = []
    for k1 in pos:
        for k2 in pos:
            if k1 < k2: # Avoid bidirectionality
                if node_distance(pos[k1],pos[k2]) < thr:
                    edges.append((k1,k2))
    return edges


# Returns a list of edges for nodes randomly (ER)
def edges_random(pos, prob = 0.01):
    edges = []
    for k1 in pos:
        for k2 in pos:
            if k1 < k2: # Avoid bidirectionality
                if np.random.rand() > 1-prob:
                    edges.append((k1,k2))
    return edges


# Returns a list of edges from the parsed ways
def edges_from_ways(pos, wlists):
    edges = []
    for w in wlists:
        for n in range(len(w)-1):
            if not w[n] in pos:
                raise Exception(f'Node {w[n]} not in the list of nodes')
            if not w[n+1] in pos:
                raise Exception(f'Node {w[n+1]} not in the list of nodes')
            edges.append((w[n],w[n+1]))
    return edges


# Returns a position dictionary of every node from the data
# 'ids' is a list of ids of every node
# 'points' is a list of tuples (lat,lon) of the position of each node
def points_pos(ids, points):
    pos = {}
    for it, n_id in enumerate(ids):
        pos[n_id] = (points[it])
    return pos


# Returns the list of nodes from the parsed xml
def get_nodes(xml):
    nodes = parsed.find_all('node')

    points = []
    lats = []
    lons = []
    ids = []
    for n in nodes:
        points.append((float(n['lat']), float(n['lon'])))
        lats.append(float(n['lat']))
        lons.append(float(n['lon']))
        ids.append(n['id'])
    return points, lats, lons, ids


# Returns the list of ways from the parsed xml
def get_ways(xml):
    ways = parsed.find_all('way')
    wlists = []
    widths = []
    for w in ways:
        # Add all the nodes the way passes through
        currw = []
        n_nodes = 0
        for c in w.findChildren('nd'):
            currw.append(c['ref'])
            n_nodes += 1
        wlists.append(currw)

        # Add the width if available
        currwidth = 0.5
        for c in w.findChildren('tag', {'k' : 'highway'}):
            if c['v'] == 'residential':
                currwidth = 2
            elif c['v'] == 'living_street':
                currwidth = 2
            elif c['v'] == 'track':
                currwidth = 1
            elif c['v'] == 'primary':
                currwidth = 5
            elif c['v'] == 'secondary':
                currwidth = 4
            elif c['v'] == 'tertiary':
                currwidth = 3

        if n_nodes > 0:
            widths += (n_nodes-1) * [currwidth]
    return wlists, widths


# Returns a parsed osm xml file
def parse_file(filename):
    with open(filename, 'r') as f:
        data = f.read()

    return BeautifulSoup(data, 'xml')


# Saves the graph to the given filename
def save_graph(graph_data, filename = 'graphSave.obj'):
    with open(filename, 'wb') as gs:
        pickle.dump(graph_data, gs)



if __name__ == '__main__':
    filename = 'data/smallcity.osm'

    parsed = parse_file(filename)

    points, lats, lons, ids = get_nodes(parsed)
    wlists, widths = get_ways(parsed)

    G = nx.Graph()
    G.add_nodes_from(ids)

    pos = points_pos(ids, points)

    #edges = edges_by_distance(pos)
    edges = edges_from_ways(pos, wlists)
    G.add_edges_from(edges)

    save_graph((G, pos, widths))
