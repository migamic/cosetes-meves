from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pickle

with open('graphSave.obj', 'rb') as gs:
    G, pos, widths = pickle.load(gs)

nx.draw(G, pos, node_size=0, width=widths)

plt.show()
