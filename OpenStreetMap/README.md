# OSM experiments

A parser and plotter for OpenStreetMap files.

`osm_parser.py` will read a `.osm` file and convert it into a graph, respecting the geographical position of each node. It gives different weights to more important roadways, etc. The output is a binary file that is then processed by the plotter.

`plotter.py` will plot the graph given by the parser.
