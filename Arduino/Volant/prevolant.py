import curses
from curses import wrapper
from random import randrange
from time import sleep


deb = "Default msg"


# Retorna la nova posicio si cal canviar de banda
def check_bounds(PPos,Wmain):
    MainH, MainW = Wmain.getmaxyx()
    if PPos <= 0: return  MainW-2  # Cal tenir en compte la box
    elif PPos >= MainW-1: return 1
    else: return PPos


# Retorna el nou status del jugador segons l'input
def change_status(c,pl_status):
    if c == curses.KEY_LEFT:
        return 2
    elif c == curses.KEY_RIGHT:
        return 1
    else:
        return pl_status # No canvia d'estat


# Retorna la nova posicio horitzontal segons el status
def move_pl(status, PPos):
    if status == 1:
        return PPos+1
    elif status == 2:
        return PPos-1
    else: return PPos # En altres casos no fer res


# Actualitza el mapa un frame
def move_map(Wmain,OPos):
    MainH, MainW = Wmain.getmaxyx()
    for pos in OPos.copy():
        OPos.remove(pos)
        lpos = list(pos)
        
        # Si queda al final substitueix per nou obstacle a dalt
        if lpos[0] < MainH-2:
            lpos[0] = lpos[0]+1
            OPos.add((lpos[0],lpos[1]))
        else:
            OPos.add((1,randrange(MainW-2)+1))


# Dibuixa tots els obstacles
def draw_map(Wmain,OPos, obst):
    for pos in OPos:
        Wmain.addstr(pos[0],pos[1],obst)


# Retorna true sii PPos es una de les posicions de OPos
def check_collision(PPos, PVPos, OPos):
    return (PVPos, PPos) in OPos


# Retorna una posicio aleatoria dins la pantalla
def generate_new_obst(Wmain):
    MainH, MainW = Wmain.getmaxyx()
    return (randrange(MainH-2)+1,randrange(MainW-2)+1) # Cal tenir en compte la box


def valid_new_pos(CandPos, PPos, OPos):
    return CandPos != PPos and not (tuple(CandPos) in OPos)


# Retorna un set de n posicions dels obstacles del mapa
def generate_map(n, PPos, Wmain):
    OPos = set()
    for i in range(n):
        CandPos = generate_new_obst(Wmain)
        while not valid_new_pos(CandPos, PPos, OPos):
            CandPos = generate_new_obst(Wmain)
        OPos.add(CandPos)
    return OPos


# Crea les 3 finestres
def crear_finestres(stdscr):
    height,width = stdscr.getmaxyx()
    SplitL = int(width/5); SplitR = int(width*4/5) # Han de sumar width
    Winfo = curses.newwin(height-1,SplitL,0,0)
    Wmain = curses.newwin(height-1,SplitR-SplitL,0,SplitL)
    Wstats = curses.newwin(height-1,SplitL,0,SplitR)
    return Winfo,Wmain,Wstats


# Escriu el text de la pantalla final
def pantalla_final(stdscr):
    stdscr.clear()
    height,width = stdscr.getmaxyx()
    stdscr.addstr(int(height/2),int(width/2)-4,'Game Over')
    stdscr.refresh()
    stdscr.nodelay(False)
    stdscr.getkey()


# Neteja totes les finestres i redibuixa els rectangles
def clear_screens(Winfo,Wmain,Wstats):
    Wmain.erase(); Wmain.box()
    Winfo.erase(); Winfo.box()
    Wstats.erase(); Wstats.box()


# Escriu la info de comandes disponibles
def write_commands(Winfo,height):
    Winfo.addstr(height,1,"Comandes:")
    Winfo.addstr(height+1,1,"\'q\' - sortir")
    Winfo.addstr(height+2,1,"\'p\'- pausa")



# Escriu tot el que cal a la finestra de info
def update_info(Winfo,PPos,deb):
    Winfo.addstr(1,1,"PPos"+str(PPos)) # Escriu la posicio
    Winfo.addstr(2,1,"Deb: " + deb)
    write_commands(Winfo,5)


# Escriu tot el que cal a la finestra main
def update_main(Wmain,OPos,obst,PPos,PVPos,player,iterat,its_obs):
    if iterat[0] >= its_obs:
        move_map(Wmain,OPos)
        iterat[0] = 0
    else: iterat[0] = iterat[0]+1

    draw_map(Wmain,OPos, obst)
    Wmain.addstr(PVPos,PPos,player)


# Escriu tot el que cal a la finestra de stats
def update_stats(Wstats,lvl,freq_pl,freq_obs,n_obs):
    Wstats.addstr(1,1,"Lvl:" + str(lvl))
    Wstats.addstr(2,1,"Velocitat jugador:" + str(freq_pl))
    Wstats.addstr(3,1,"Velocitat obstacles:" + str(int(freq_obs / freq_pl)))
    Wstats.addstr(4,1,"# d'objectes:" + str(n_obs))


# Escriu la pantalla de pausa
def pause_screen(stdscr):
    stdscr.clear()
    height,width = stdscr.getmaxyx()
    msg1 = 'Pausa'
    msg2 = 'Prem qualsevol tecla per continuar'
    stdscr.addstr(int(height/2),int(width/2 - len(msg1)/2),msg1)
    stdscr.addstr(int(height/2)+1,int(width/2 - len(msg2)/2),msg2)
    stdscr.refresh()
    stdscr.nodelay(False)
    stdscr.getkey()
    stdscr.nodelay(True)


def main(stdscr):
    global deb

    # Setup de curses
    stdscr.clear() # Neteja la pantalla
    curses.curs_set(0) # No mostrar el cursor
    curses.use_default_colors() # Transparencia a la terminal
    stdscr.nodelay(True) # No esperar input de l'ususari

    # Escriu el meu nom
    nom = "Jaume Ros Alonso - 2020"
    stdscr.addstr(stdscr.getmaxyx()[0]-1,int(stdscr.getmaxyx()[1]/2 - len(nom)/2),nom)

    Winfo,Wmain,Wstats = crear_finestres(stdscr)

    # Setup inicial
    player = 'A'
    obst = 'X'
    MainH, MainW = Wmain.getmaxyx()
    PVPos = MainH-5 # Posicio vertical, constant
    PPos = int(MainW/2) # Posicio horitzontal
    OPos = generate_map(10, PPos, Wmain)
    deb = "Map Generated"
    pl_status = 0 # 0: quiet; 1: dreta; 2: esquerra

    lvl = 1
    vel_pl = 20 # 1/vel es la frequencia d'update
    iterat = [0] # En llista per a passar per ref
    its_obs = 10 # Nre d'iteracions que han de passar per a que es moguin
    
    # Anar fent segons el input
    while True:
        clear_screens(Winfo,Wmain,Wstats)
        update_info(Winfo,PPos,deb)
        update_main(Wmain,OPos,obst,PPos,PVPos,player,iterat,its_obs)
        update_stats(Wstats,lvl,vel_pl,its_obs,len(OPos))
        Winfo.refresh(); Wmain.refresh(); Wstats.refresh()
        deb = str(pl_status)

        if check_collision(PPos, PVPos, OPos): break

        # Agafa input
        c = stdscr.getch()
        if c == ord('q'):
            break # Sortir
        elif c==ord('p'):
            pause_screen(stdscr)
        else:
            pl_status = change_status(c,pl_status)
        
        # Mou el jugador
        PPos = move_pl(pl_status,PPos)
        PPos = check_bounds(PPos,Wmain)

        sleep(1/vel_pl)


    pantalla_final(stdscr)


wrapper(main)
