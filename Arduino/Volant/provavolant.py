import serial
from time import sleep

port = '/dev/ttyUSB0'
arduino = serial.Serial(port,9600,timeout=1)

while (True):
    arduino.write("r".encode('utf-8'))
    print(str(arduino.readline()))
