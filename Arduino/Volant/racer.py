import curses
from curses import wrapper
from random import randrange
from time import sleep

import serial # Llegir el volant


deb = "Default msg"


# Retorna la posicio limit si el jugador es passa, la original si no
def check_bounds(PPos,Wmain):
    MainH, MainW = Wmain.getmaxyx()
    if PPos <= 1: return  1  # Cal tenir en compte la box
    elif PPos >= MainW-2: return MainW-2

    return PPos


# Retorna el nou status del jugador segons l'input
def change_status(c,pl_status):
    if c == curses.KEY_LEFT:
        return 2
    elif c == curses.KEY_RIGHT:
        return 1
    else:
        return pl_status # No canvia d'estat

# Actualitza el nivell i l'amplitud cada n punts
def change_level(punts, lvl, ample):
    n = 500
    if punts[0] % n == 0:
        return lvl+1, ample-1
    return lvl, ample


# Retorna l'increment de posicio del jugador segons l'estat del volant
def pos_inc(ardinp, Wmain):
    Sensibilitat = 0.001 # Valor que va be, potser passar com a opcio
    absinc = abs(ardinp)**(1/1) * Sensibilitat
    if ardinp >= 0: return absinc
    else: return -absinc


# Retorna la nova posicio horitzontal segons el input del volant
# Assumeix que el input va de [-512,512]
def move_pl(ardinp, Wmain, PPos):
    newPos = PPos + pos_inc(ardinp,Wmain)

    return check_bounds(newPos,Wmain)

# Actualitza el mapa un frame
def move_map(Wmain, OPos, ample):
    MainH, MainW = Wmain.getmaxyx()
    for i in range(len(OPos)-1, 0, -1): # Iterar al reves
        OPos[i] = OPos[i-1]
    canvidestat = randrange(0,10) == 1 # 1/n de probabilitat
    deb = canvidestat
    inc = OPos[1]-OPos[2] # +1 si la corva va cap a l'esquerra
    if not canvidestat:
        OPos[0] = OPos[0]+inc # Mante la trajectoria
    else:
        if inc == 0:
            OPos[0] = OPos[0] + randrange(0,2)*2-1
        # else es queda igual
    
    # Evitar que surti del mapa
    if OPos[0] < 1:
        OPos[0] = 1
    elif OPos[0] > MainH-2:
        OPos[0] = MainH-2
    


# Dibuixa tots els obstacles
def draw_map(Wmain, OPos, obst, ample):
    for i in range(len(OPos)):
        Wmain.addstr(i+1,OPos[i],obst)
        Wmain.addstr(i+1,OPos[i]+ample,obst)


# Retorna true sii PPos es una de les posicions de OPos
def check_collision(PPos, PVPos, OPos, ample):
    return round(PPos) <  OPos[PVPos] or round(PPos) > OPos[PVPos]+ample


# Retorna una posicio aleatoria dins la pantalla
def generate_new_obst(Wmain):
    MainH, MainW = Wmain.getmaxyx()
    return (randrange(MainH-2)+1,randrange(MainW-2)+1) # Cal tenir en compte la box


# Dibuixa la carretera inicial (recta (recta)
def generate_map(Wmain, ample):
    MainH, MainW = Wmain.getmaxyx()
    OPos = [round((MainW/2)-(ample/2))-1] * (MainH-2) # Tenint en compte la box
    return OPos


# Crea les 3 finestres
def crear_finestres(stdscr):
    height,width = stdscr.getmaxyx()
    SplitL = int(width/5); SplitR = int(width*4/5) # Han de sumar width
    Winfo = curses.newwin(height-1,SplitL,0,0)
    Wmain = curses.newwin(height-1,SplitR-SplitL,0,SplitL)
    Wstats = curses.newwin(height-1,SplitL,0,SplitR)
    return Winfo,Wmain,Wstats


# Escriu el text de la pantalla final
def pantalla_final(stdscr,punts):
    stdscr.clear()
    height,width = stdscr.getmaxyx()
    stdscr.addstr(int(height/2),int(width/2)-5,'Game Over')
    stdscr.addstr(int(height/2)+2,int(width/2)-5,'Punts: ' + str(punts))
    stdscr.refresh()
    stdscr.nodelay(False)
    stdscr.getkey()


# Neteja totes les finestres i redibuixa els rectangles
def clear_screens(Winfo,Wmain,Wstats):
    Wmain.erase(); Wmain.box()
    Winfo.erase(); Winfo.box()
    Wstats.erase(); Wstats.box()


# Escriu la info de comandes disponibles
def write_commands(Winfo,height):
    Winfo.addstr(height,1,"Comandes:")
    Winfo.addstr(height+1,1,"\'q\' - sortir")
    Winfo.addstr(height+2,1,"\'p\'- pausa")



# Escriu tot el que cal a la finestra de info
def update_info(Winfo,PPos,deb):
    Winfo.addstr(1,1,"PPos"+str(round(PPos,2))) # Escriu la posicio
    Winfo.addstr(2,1,"Deb: " + str(deb))
    write_commands(Winfo,5)


# Escriu tot el que cal a la finestra main
def update_main(Wmain,OPos,obst,PPos,PVPos,player,iterat,its_obs,ample,punts):
    if iterat[0] >= its_obs:
        move_map(Wmain,OPos,ample)
        iterat[0] = 0
    else:
        iterat[0] += 1
        punts[0] += 1

    draw_map(Wmain, OPos, obst, ample)
    Wmain.addstr(PVPos,round(PPos),player)


# Escriu tot el que cal a la finestra de stats
def update_stats(Wstats,lvl,freq_pl,freq_obs,ample,puntsint):
    Wstats.addstr(1,1,"Lvl:" + str(lvl))
    Wstats.addstr(2,1,"Velocitat jugador: " + str(freq_pl))
    Wstats.addstr(3,1,"Velocitat obstacles: " + str(round(1/freq_obs,2)) + "Hz")
    Wstats.addstr(4,1,"Amplitud carretera: " + str(ample))
    Wstats.addstr(10,1,"Punts: " + str(puntsint))


# Escriu la pantalla de pausa
def pause_screen(stdscr):
    stdscr.clear()
    height,width = stdscr.getmaxyx()
    msg1 = 'Pausa'
    msg2 = 'Prem qualsevol tecla per continuar'
    stdscr.addstr(int(height/2),int(width/2 - len(msg1)/2),msg1)
    stdscr.addstr(int(height/2)+1,int(width/2 - len(msg2)/2),msg2)
    stdscr.refresh()
    stdscr.nodelay(False)
    stdscr.getkey()
    stdscr.nodelay(True)


def main(stdscr):
    global deb

    # Setup de curses
    stdscr.clear() # Neteja la pantalla
    curses.curs_set(0) # No mostrar el cursor
    curses.use_default_colors() # Transparencia a la terminal
    stdscr.nodelay(True) # No esperar input de l'ususari

    # Setup del arduino
    port = port = '/dev/ttyUSB0'
    arduino = serial.Serial(port,9600,timeout=1) 

    # Escriu el meu nom
    nom = "Jaume Ros Alonso - 2020"
    stdscr.addstr(stdscr.getmaxyx()[0]-1,int(stdscr.getmaxyx()[1]/2 - len(nom)/2),nom)

    Winfo,Wmain,Wstats = crear_finestres(stdscr)

    # Setup inicial
    player = 'A'
    obst = 'X'
    MainH, MainW = Wmain.getmaxyx()
    PVPos = MainH-5 # Posicio vertical, constant
    PPos = int(MainW/2) # Posicio horitzontal incial
    pl_status = 0 # 0: quiet; 1: dreta; 2: esquerra
    punts = [0] # En llista per a passar per ref

    lvl = 1
    vel_pl = 200 # 1/vel es la frequencia d'update
    iterat = [0] # En llista per a passar per ref
    its_obs = 3 # Nre d'iteracions que han de passar per a que es moguin
    ample = 30 # Distancia entre limits de la carretera
 
    OPos = generate_map(Wmain, ample)
    deb = "Map Generated"
    
    # Anar fent segons el input
    while True:
        clear_screens(Winfo,Wmain,Wstats)
        update_info(Winfo,PPos,deb)
        update_main(Wmain,OPos,obst,PPos,PVPos,player,iterat,its_obs,ample,punts)
        update_stats(Wstats,lvl,vel_pl,its_obs,ample,punts[0])
        Winfo.refresh(); Wmain.refresh(); Wstats.refresh()

        if check_collision(PPos, PVPos, OPos, ample): break

        lvl, ample = change_level(punts, lvl, ample)

        # Agafa input de teclat
        c = stdscr.getch()
        if c == ord('q'):
            break # Sortir
        elif c==ord('p'):
            pause_screen(stdscr)
        
        # Agafa input de l'arduino
        ardinp = str(arduino.readline())[2:-5]
        if (ardinp == ""):
            ardinp = 0
        else:
            ardinp = int(ardinp) - 512 # Per a que vagi de [-512,512]
        deb = ardinp

        # Mou el jugador
        PPos = move_pl(ardinp, Wmain, PPos)

        # Demanar posicio de input a l'arduino
        arduino.write("r".encode('utf-8'))
        sleep(1/vel_pl)


    arduino.write("m".encode('utf-8'))
    pantalla_final(stdscr,punts[0])


wrapper(main)
