#include <LiquidCrystal.h>

// Des del punt de vista de l'entrada de la regleta negra
const int releDre = 2;
const int releEsq = 3;

const int botoDre = 4;
const int botoEsq = 5;


const int LCD_RS = 6;
const int LCD_EN = 7;
const int LCD_D4 = 8;
const int LCD_D5 = 9;
const int LCD_D6 = 10;
const int LCD_D7 = 11;
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);

void setup() {
    // initialize serial communication at 9600 bits per second:
    Serial.begin(9600);
    // make the pushbutton's pin an input:
    pinMode(releDre, OUTPUT);
    pinMode(releEsq, OUTPUT);
    pinMode(botoDre, INPUT_PULLUP);
    pinMode(botoEsq, INPUT_PULLUP);

    // Setup del LCD
    lcd.begin(16, 2);
    lcd.setCursor(0, 0);         // move cursor to   (0, 0)
    lcd.print("Arduino");        // print message at (0, 0)
    lcd.setCursor(2, 1);         // move cursor to   (2, 1)
    lcd.print("GetStarted.com"); // print message at (2, 1)
}


void loop() {

    if (digitalRead(botoDre) == HIGH) {
        digitalWrite(releDre, LOW);
        Serial.println("DRE off");
    } else {
        digitalWrite(releDre, HIGH);
        Serial.println("DRE on");
    }

    if (digitalRead(botoEsq) == HIGH) {
        digitalWrite(releEsq, LOW);
        Serial.println("ESQ off");
    } else {
        digitalWrite(releEsq, HIGH);
        Serial.println("ESQ on");
    }

    delay(100); // Per a no saturar l'Arduino
}
