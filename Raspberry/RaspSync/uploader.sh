#!/bin/bash

# Comprimeix un fitxer o directori i el puja a la carpeta de la raspberry

nomarxiu='comprupload.tar.gz'
iprasp='192.168.1.55' # Fallback si raspberrypi.local no funciona

if ping -c1 -w3 raspberrypi.local >/dev/null 2>&1
then
    iprasp='raspberrypi.local'
fi

echo Comprimint a $nomarxiu
tar -czf $nomarxiu "$@"

echo Pujant a la raspberry
scp $nomarxiu pi@"$iprasp":./Jaume/backup/$nomarxiu

echo Netejant
rm $nomarxiu
