#!/bin/bash

# Comprimeix un fitxer o directori i el puja a la carpeta de la raspberry

nomarxiu='comprupload.tar.gz' # Que sigui el mateix que al uploader
iprasp='192.168.1.55' # Fallback si raspberrypi.local no funciona

if ping -c1 -w3 raspberrypi.local >/dev/null 2>&1
then
    iprasp='raspberrypi.local'
fi

echo Descarregant de la raspberry
scp pi@"$iprasp":./Jaume/backup/$nomarxiu .

echo Descomprimint
tar -xzf $nomarxiu

echo Netejant
rm $nomarxiu
