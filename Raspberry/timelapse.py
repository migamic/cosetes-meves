# A small Python program that allows the user to record timelapses with the Raspberry Pi

from picamera import PiCamera # Official library to control the camera
from time import sleep

try:
    fps=int(raw_input('Images per second:'))
    real=int(raw_input('Real time (seconds):'))
    final=int(raw_input('Final time (seconds):'))
except ValueError:
    print "Not a number"

N = final*fps
S = real/N

camera = PiCamera()
camera.rotation = 180
camera.resolution = (1920, 1080)
camera.framerate = 15
camera.start_preview()
for i in range(N):
    sleep(S)
    camera.capture('./Timelapse/image%s.jpg' % i)
    print("Image",i)
camera.stop_preview()
